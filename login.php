    <?php
        	require_once("db/config.php");
        	require_once("db/connect.php");
            require_once("db/func.php"); 
        	session_start(); 


            $error_message = "" ;
            if(isset($_COOKIE[USER_ID]))
        	{
        		header("Location: index.php"); 
        	}

            if(isset($_SESSION[REQUESTED_PAGE]))
                {
                    $page = $_SESSION[REQUESTED_PAGE]; 
                }

        	if($_SERVER["REQUEST_METHOD"] == "POST")
        	{
        		$user_name = $_POST[USER_NAME]; 
        		$user_password = $_POST[USER_PASSWORD]; 
                
        		$user_id = login_user($user_name,$user_password); 
        		if($user_id != null)
                {

                    setcookie(USER_ID,$user_id); 
            		if(isset($page))
                    {
                        header("Location: ".$page); 

                    }
                    else
                    {
                      header("Location: index.php"); 
            	
                    }
                }

                else
                {
                    $error_message = "The email/screen or password provided is incorrect"; 
                }
            } 
        		


        ?>





    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        

        <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/freelancer.css" rel="stylesheet">
        <link href="css/login.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

     

    </head>



        <!-- Contact Section -->
       <section>
                <div class="row">
                    <div class=" text-center">
                        <h2>ShareMate</h2>
                        <h4> Apps you can Share </h4>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        
                        <form  method="post" id="login-form" >

                            <div id="error-message">
                                <?php 
                                    echo $error_message;
                                ?>
                            </div>
                           
                         <div class="row control-group login">
                                <div class="form-group col-xs-12 floating-label-form-group controls">
                                    <label>Screen Name or E-mail</label>
                                    <input type="text" name="<?php echo USER_NAME; ?>" value="<?php if(isset($user_name)) echo $user_name; ?>"placeholder="Screen Name or E-mail"  required >
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>

                             <div class="row control-group login">
                                <div class="form-group col-xs-12 floating-label-form-group controls">
                                    <label>Password</label>
                                    <input type="password" name="<?php echo USER_PASSWORD ?>" placeholder="Password"  required>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>

                             <div class="row login">
                                <div class="form-group col-xs-12">
                                    <button type="submit" id="login-button"class="btn btn-success btn-lg" >Login  </button>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="register-link col-xs-12">
                                    <a href="register.php">Register </a>
                                </div>
                            </div>
                            <div class="row"> 
                                   <div id="forgot-password"class=" col-xs-12">
                                    <a href="recover.php">Forgot your password?</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


        </section>



        
       

        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script> 

        <!-- Contact Form JavaScript -->
        <script src="js/jqBootstrapValidation.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/freelancer.js"></script>


    </html>
