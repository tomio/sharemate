<?php 

	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("db/func.php");   
	require_once("inc/header.php");

  $page = "billing.php"; 
  require_once("inc/menu.php"); 

  $user_id = $_COOKIE[USER_ID]; 
  $subs = get_subbed_apps($user_id);

  $no_apps = false; 
  if(count($subs) == 0)
    $no_apps = true; 


?>


 <link href="css/billing.css" rel="stylesheet">

 <?php 
      if($no_apps){
        ?>
  <h3 id="no-apps">
    You are not currently sharing any apps 
 </h3>
 <?php } ?>



<?php
  foreach ($subs as $sub)
    { 

      $app = get_app_info($sub[APP_ID]);
      $next_bill_date = $sub[NEXT_BILL];
      $last_bill_date = $sub[LAST_BILL];

 ?>

<div class="app-description ">
<ul class='item-list'>
  <li class='item'>
    <div class='item__information'>
      <div class='item__image'>
        <img src="<?php echo 'img/'.strtolower($app[APP_NAME])  .'.png'; ?>">
      </div>
      <div class='item__body'>
        <h2 class='item__title'><?php echo $app[APP_NAME]; ?> Month Subscription</h2>
        <p class='item__description'>
        	Last Bill date 
          <b> 
            <?php echo $last_bill_date; ?>
          </b>
        </p>

        <p class='item__description'>
        	Next Bill date 
          <b> 
          	<?php
                if($next_bill_date != null)
                {
                  if($sub[RECUR] == WILL_RECUR)
                  {
                    echo $next_bill_date; 

                  }
                  elseif($sub[RECUR] == NOT_RECUR)
                  { 
                    echo "Cancelled"; 
                  }
                }
                else
                  echo "Undetermined"; 
             ?>
          </b>

           <p>
          App Price:
          <b> 
             $<?php echo $app[APP_PRICE]; ?>
          </b>
        </p>

          <?php 
              if($next_bill_date != null && !($sub[RECUR] == NOT_RECUR))
              {

                echo "<br>
                      <a href='cancel.php?".APP_ID."=".$sub[APP_ID]."'> Cancel Subscription </a>";  
              }
          ?>


        </p>
       
      </div>
    </div>
     
  
  </li>
</ul>
</div>


<?php 
  }
?>