<?php 

	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("db/func.php"); 

	if(!isset($_GET[MATCH_ID]))
		header("Location: index.php"); 
	$pending_match_id = $_GET[MATCH_ID]; 
	$page = "pending.php?".MATCH_ID."=".$pending_match_id; 

	require_once("inc/header.php");

	$user_id = $_COOKIE[USER_ID];  
	$match_info = get_match_info($pending_match_id); 
	$app_id = $match_info[APP_ID]; 
	
	if($user_id == $match_info[USER_ONE])
	{	
		$other_user_id = $match_info[USER_TWO]; 
	}

	elseif($user_id == $match_info[USER_TWO])
	{
		$other_user_id = $match_info[USER_ONE]; 
	}
	else
	{
		header("Location: index.php"); 
	}

	$mate = get_user_info($other_user_id); 


	if($_SERVER["REQUEST_METHOD"] == "POST")
	{	
			
		$response = $_POST["user_response"]; 
		if($response == RESPONSE_ACCEPTED)
		{	
			$response_status = update_user_response($user_id,$pending_match_id,$app_id); 
			
			if($response_status == BOTH_ACCEPTED)
			{
				// send email to other user 
				// show successfully matching message
			    send_email("Account credentials need to be created","tomiokiji@gmail.com","Account credential need to be created");
				send_match_made_email($other_user_id,$user_id,$app_id); 
				header("Location: both_accepted.php?".USER_ID."=".$other_user_id."&".APP_ID."=".$app_id); 
				
			}
			else
			{
				// wiating on rsponse message 
				header("Location: waiting.php"); 


			}

		}
		else if($response == RESPONSE_DECLINED)
		{
			// send an email to declined user  
			// remove users from pending matches table  
			send_match_declined_email($user_id,$other_user_id,$app_id); 
			deny_user($pending_match_id);
			header("Location: response_declined.php"); 


		}

	 
	}

	

?>
    <link href="css/pending.css" rel="stylesheet">
	<h2 id="first"> We have found ShareMate for you  </h2>

<div id="match-info">

		<form method="post">
			<div class=" match-user-info container">

				<p class="row">ShareMate name: <b><?php echo $mate[USER_NAME]; ?> </b></p>

				<?php 

					if($mate[AMOUNT_SHARED] == 0)
					{
				?>
					<p class="row"> <b><?php echo $mate[USER_NAME]; ?> </b>is  a first time sharemate!!!  </p>	
				<?php 
					}

					else{
				?>
					<p class="row"> Amount of Sharemate had in the past: <b><?php echo $mate[AMOUNT_SHARED]; ?>  </b> </p>	
				<?php }?>


			</div>
			
				<p id="accept-text"> Do you Accept ?</p>
			<div class="container">
				<div class="row buttons">
					<button name="user_response" value="<?php echo RESPONSE_ACCEPTED ; ?>"type="submit" class="col-xs-2 btn btn-lrg btn-success ">Accept </button> 
					<button name="user_response" value="<?php echo RESPONSE_DECLINED; ?>"type="submit" id="decline-button" class="col-xs-2 btn btn-lrg btn-info ">Decline </button> 
				</div>
				<br>
				<p id="accept-text"> OR</p>
				<br> 

				<a id="search-link" href="<?php echo 'search_user.php?'.MATCH_ID.'='.$pending_match_id.'&other='.$other_user_id; ?>"> Search for a potentianl Sharemate </a>

			
				

			</div>


		</form> 

	</div>

