<?php 
	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("inc/header.php"); 

?>


<link rel="stylesheet" type="text/css" href="css/message.css">

<div class="message"> 
<p> 
	<b> 
		Thank you for responding. You will be getting a response very shortly, 
		so be on the lookout !!!
		<br> 
		Please be sure to check the status of your app using the link below 
	</b> 
</p> 
<a href="my_apps.php"> Check Status </a> 
</div> 