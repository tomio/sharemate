<?php 

	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("db/func.php");
	require_once("vendor/autoload.php"); 

	


	\Stripe\Stripe::setApiKey(PRIVATE_KEY); 


	$i = 0; 
	$subs = get_all_bill_dates(); 
	foreach($subs as $sub)
	{


		if(is_today($sub[NEXT_BILL]))
		{



			$app = get_app_info($sub[APP_ID]); 

			$user_id = $sub[USER_ID]; 
			$app_id = $sub[APP_ID]; 	


			if(!next_bill_not_null($user_id,$app_id))
				{
					continue; 
				}

				
			if(cancelled_sub($user_id,$app_id))
			{
				// then move the sharemate to the  users waiting table 
				// set user_id next bill to null 

				// send an email to the sharemate 
				// remove user from sharers  

				// check to see if the users card was declined
				// if so, remove user from subscriptions table 

				// send a card declined email 

				// charge the sharemate 


				$mate_info = get_sharemate_sub_info($user_id,$app_id); 
				remove_sub_date($user_id,$app_id); 

				// if the mate has not already been charged 


				if($mate_info)
				{
					$mate_id = $mate_info[USER_ID]; 
					$mate_stripe_id = $mate_info[STRIPE_ID]; 


					// if both user cancell subscription 
					if(cancelled_sub($mate_id,$app_id))
					{	
						// remove both subs 
						// from db 
						remove_sharers($user_id,$app_id); 

					
						remove_sub_date($mate_id,$app_id); 

						
						continue; 

					}
					


					
					try
					{
						\Stripe\Charge::create(array(

						"amount" => $app[APP_PRICE]*100,
						"currency" =>"usd",
						"description"=>"user charge",
						"customer" => $mate_stripe_id
						));


						remove_sharers($user_id,$app_id); 


						add_user_waiting($mate_id,$app_id,"",false,true);
					
						

						// set the next bill date to null 
						// so the user is not charged while not 
						// having an account 
						set_next_bill_null($mate_id,$app_id); 

						// send sharemate cancelled email 	
						// to mate 
						// send acount canccelled to user
					    send_sub_cancelled_email($mate_id,$app_id); 

					}

 					catch(\Stripe\Error\Card $e) 	
 					{
						remove_sharers($user_id,$app_id); 

						// send card declined email
						// to both users 
						remove_sub_date($mate_id,$app_id); 

						send_card_declined_email($mate_id,$app_id); 


					}


				}

				else
				{

					$mate_id = get_sharemate_info($user_id,$app_id); 
					remove_sharers($user_id,$app_id); 

					// help ensure that a user whose card is declined is not 
					// getting a month of a service free 

					if(user_still_suscribed($mate_id,$app_id))
					{
						// set the next bill date to null so that the user is not charged while 
						// he does not have a subscription 

						// ensure that the sharemate is not 
						// being charged for a subscription that 
						// does not exist 

						set_next_bill_null($mate_id,$app_id); 
						
						// then move taht mate to users waiting
						if(!already_waiting($mate_id,$app_id))
							add_user_waiting($mate_id,$app_id,"",false,true);
						// delete the sharers column 
						// send email to mate 

						send_sub_cancelled_email($mate_id,$app_id);
					}
				}


				continue; 

			}

			else
			{


				$mate_id = get_sharemate_info($user_id,$app_id);
				if(cancelled_sub($mate_id,$app_id))
				{

					$stripe_id = $sub[STRIPE_ID]; 

					try
					{
						// charge the user 


						\Stripe\Charge::create(array(

						"amount" => $app[APP_PRICE]*100,
						"currency" =>"usd",
						"description"=>"user charge",
						"customer" => $stripe_id
						));


						// remove from sharers table 
						//remove_sharers($user_id,$app_id); 


						// then move the sharemate to the  users waiting table 
						if(!already_waiting($user_id,$app_id))
							add_user_waiting($user_id,$app_id,"",false,true);
					
						

						// set user_id next bill to null 

						set_next_bill_null($user_id,$app_id); 

						// send sharemate cancelled email 	
						// to mate 
						// send acount canccelled to user
					    send_sub_cancelled_email($user_id,$app_id); 



					}


 					catch(\Stripe\Error\Card $e) 
					{
					
						//remove_sharers($user_id,$app_id); 
 
						remove_sub_date($user_id,$app_id); 

						send_card_declined_email($user_id,$app_id); 


					}
					 

					continue; 

				}
			}



				// ensure that the state of the subscription has not changed
				// while proccessing 
				
			$stripe_id = $sub[STRIPE_ID]; 

			try{
					\Stripe\Charge::create(array(

					"amount" => $app[APP_PRICE]*100,
					"currency" =>"usd",
					"description"=>"user charge",
					"customer" => $stripe_id
					));


				update_bill_date($user_id,$app_id); 

 				$mate_id =  get_sharemate_id($user_id,$app_id); 

 				$next_bill = get_next_bill($mate_id,$app_id); 
 				if(!is_today($next_bill))
 				{	 

 					echo "What the id should be: ".$mate_id;  
 					notify_reload($user_id,$mate_id,$app_id);
 					
 				}

				// check if the sharemate has been charged successfulyy 
				// if so notify that the account needs to be reloaded 	
				$i++;

 				echo "got here ".$i++; 
 				echo "<br>"; 

			}


			 catch(\Stripe\Error\Card $e) 
			{	
				// send card declined email to user whose 
				// card was declined 

				send_card_declined_email($user_id,$app_id); 

				// charge the Sharemate  since the charge was declined 


				$mate_info = get_sharemate_sub_info($user_id,$app_id); 
				remove_sub_date($user_id,$app_id); 
				// check if the mate has not already been charged 
				if($mate_info)
				{
					$mate_id = $mate_info[USER_ID]; 
					$mate_stripe_id = $mate_info[STRIPE_ID]; 
					
					try
					{
						\Stripe\Charge::create(array(

						"amount" => $app[APP_PRICE]*100,
						"currency" =>"usd",
						"description"=>"user charge",
						"customer" => $mate_stripe_id
						));


						remove_sharers($user_id,$app_id); 


						add_user_waiting($mate_id,$app_id,"",false,true);
					
						

						// set the next bill date to null 
						// so the user is not charged while not 
						// having an account 
						set_next_bill_null($mate_id,$app_id); 

						// send sharemate cancelled email 	
						// to mate 
						// send acount canccelled to user
					    send_sub_cancelled_email($mate_id,$app_id); 

					}

					catch(\Stripe\Error\Card $e) 
					{
						remove_sharers($user_id,$app_id); 

						// send card declined email
						// to both users 
						remove_sub_date($mate_id,$app_id); 

						send_card_declined_email($mate_id,$app_id); 


					}


				}

				else
				{
					// case the user is charged and his sharemate has already been charged 



					// get the  mate id  id 

					$mate_id = get_sharemate_info($user_id,$app_id); 
					// set the next bill date to null so that the user is not charged while 
					// he does not have a subscription 

					set_next_bill_null($mate_id,$app_id); 
					// then move taht mate to users waiting

					add_user_waiting($mate_id,$app_id,"",false,true);
					// delete the sharers column 

					remove_sharers($user_id,$app_id); 
					// send email to mate 

					send_sub_cancelled_email($mate_id,$app_id);  

					
				}


			}

		}
	}



?>