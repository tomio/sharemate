<?php
	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("db/func.php");   

    $no_sign_in = true; 
	require_once("inc/header.php"); 

    
	$apps = get_apps(); 
    ?>  


<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
   

    <!-- Custom CSS -->
    <link href="css/index.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

 

</head>

                <div class="first">
                    <h1>
                        Apps you can share 
                    </h1>
                </div>

                <div class="apps">                 
      			<?php 

                foreach($apps as $app) 
      			{?>
                
                <div class=" app col-sm-2 grid">
           	
                    <a href="<?php echo 'checkout.php?'.APP_ID.'='.$app[APP_ID]; ?>">
                       
                        <img src="<?php echo 'img/'.strtolower($app[APP_NAME]).'.png'; ?>" class="img-responsive" alt="">
                     </a>
                    <a href="<?php echo 'checkout.php?'.APP_ID.'='.$app[APP_ID]; ?>" class="app-price row" >
                    	<?php echo "$".$app[APP_PRICE]."/month"; ?>
                    </a>
                    <a href="<?php echo 'checkout.php?'.APP_ID.'='.$app[APP_ID]; ?>"  >
                    
                    <p class="app-desc ">
                        <?php echo $app[APP_DESCRIPTION]; ?> 

                    </p>
                   </a>

                 <a href="<?php echo 'checkout.php?'.APP_ID.'='.$app[APP_ID]; ?>">
                    Get a ShareMate          
                 </a>
                   

              </div>


            </div>

            	<?php }?>



  
    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script> 

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/freelancer.js"></script>


