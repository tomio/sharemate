<?php
	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("db/func.php");   
	
	$is_sharer = false; 
	$user_id = $_COOKIE[USER_ID]; 

	if(isset($_GET[SHARER_ID]))
	{	
		$sharer_id = $_GET[SHARER_ID]; 
		$is_sharer = true; 

	}
	elseif(isset($_GET["month"]) && isset($_GET[APP_ID]))
	{
		$app_id = $_GET[APP_ID]; 
	}

	else
	{
		header("Location: my_apps.php"); 
	}



	if($is_sharer)
		$info = get_shared_account_info($sharer_id); 
	else
		$info = get_free_month_credentials($user_id,$app_id); 


	if($info == null)
		header("Location: my_apps.php"); 
	if($is_sharer && $user_id != $info[USER_ONE] && $user_id != $info[USER_TWO])
	{
		header("Location: my_apps.php"); 

	}

	$page="credentials.php?".SHARER_ID."=".$sharer_id;  

	require_once("inc/header.php");


	if($is_sharer)
		$app_name = get_app_info($info[APP_ID])[APP_NAME]; 
	else
		$app_name = get_app_info($app_id)[APP_NAME];

?>



<link rel="stylesheet" type="text/css" href="css/message.css">

<div class="message"> 
<p> 
	<?php echo  $app_name; ?> Email: 
	<br>
	<b> 
		<?php echo $info[SHARED_EMAIL]; ?>
	</b> 
	<br>
	<?php echo  $app_name; ?>  Password: 
	<br> 
	<b>
		<?php echo $info[SHARED_PASSWORD]; ?>
	</b>
</p> 
</div> 