<?php 
	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("db/func.php");   

  	require_once("vendor/autoload.php"); 
	\Stripe\Stripe::setApiKey(PRIVATE_KEY); 
	



if(isset($_POST["stripeToken"]) )
{	
	$user_id = $_COOKIE[USER_ID]; 
	$user_info = get_user_info($user_id); 
	$app_id = $_POST[APP_ID]; 
	$token = $_POST["stripeToken"]; 	
	$app = get_app_info($app_id); 
	try 
	{	


		
			$customer = \Stripe\Customer::create(array(
  			"source" => $token,
  			"description" => $user_info[USER_NAME] 
			)); 

			$stripe_id = $customer->id; 
			set_stripe_id($user_id,$app_id,$stripe_id);


		



		$charge = \Stripe\Charge::create(array(


			"amount" => $app[APP_PRICE]*100,
			"currency" =>"usd",
			"description"=>"user charge",
			"customer" => $stripe_id
			)); 


		// add payment info 

		// add the user to pending request 

		$match_ids = get_match($app_id);

		if(count($match_ids) > 0)
		{
			foreach($match_ids as $id)
			{	
				// ensure that the user who is matched
				//is not the same person 
				if($id != $user_id && !already_denied($user_id,$id,$app_id))
				{	
					$match_id = $id; 
					break;
				}	
			}

		}


		if($match_ids != false && isset($match_id))
		{

			$pending_match_id = add_pending_match($user_id,$match_id,$app_id);
			add_sub_date($user_id,$app_id);
			set_stripe_id($user_id,$app_id,$stripe_id); 

			send_match_found_mail($pending_match_id,$match_id,$app_id); 
			header("Location: pending.php?".MATCH_ID."=".$pending_match_id); 

		}

		else
		{	
			add_user_waiting($user_id,$app_id); 
			set_stripe_id($user_id,$app_id,$stripe_id); 
			set_stripe_charge($user_id,$app_id,$charge["id"]); 
			header("Location: confirmation.php"); 
		}
	}

		

	catch(Stripe_CardError $e)
	{

	}
}

