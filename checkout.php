<?php 
	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("db/func.php");   
	require_once("inc/header.php"); 
  require_once("vendor/autoload.php"); 

  $app_id = $_GET[APP_ID]; 
  $user_id = $_COOKIE[USER_ID]; 

  
  if(already_purchased($user_id,$app_id))
     header("Location: already_purchased.php"); 

  $user_email = get_user_email($user_id); 
  $app= get_app_info($app_id); 




?>
   


   <link href="css/checkout.css" rel="stylesheet">

<div class="app-description ">
<ul class='item-list'>
  <li class='item'>
    <div class='item__information'>
      <div class='item__image'>
        <img src="<?php echo 'img/'.strtolower($app[APP_NAME])  .'.png'; ?>">
      </div>
      <div class='item__body'>
        <h2 class='item__title'><?php echo format_app_name($app[APP_NAME]); ?> Month Subscription</h2>
        <p class='item__description'><?php echo $app[APP_DESCRIPTION]; ?>
          <b> Remember if we are not able to find you a match within 24 hours,
              you get the whole month free ! 
        </p>
      </div>
      <div class='item__price js-item-price'>$<?php echo $app[APP_PRICE]; ?></div>
    </div>
     
     <form class="checkout-button" action="process.php" method="POST">
        <input type="hidden" name="<?php echo APP_ID; ?>" value="<?php echo $app[APP_ID]; ?>">  
        <script
          src="https://checkout.stripe.com/checkout.js" class="stripe-button"
          data-key="<?php echo STRIPE_KEY;?>"
          data-image="<?php echo 'img/'.strtolower($app[APP_NAME]).'.png'; ?>"
          data-name="<?php echo $app[APP_NAME]; ?>  " 
          data-description="2 widgets"
          data-amount="<?php echo $app[APP_PRICE] *100?>"
          data-email="<?php echo $user_email; ?>"
          data-locale="auto">
        </script>
        </form>
  </li>
</ul>
</div>



    

</ul>

