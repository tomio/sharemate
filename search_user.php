<?php
    require_once("db/config.php");
    require_once("db/connect.php");
    require_once("db/func.php");   
    require_once("inc/header.php"); 




    if(isset($_GET[APP_ID]))
    {
        $app_id = $_GET[APP_ID]; 

    }

    elseif(isset($_GET[MATCH_ID]) && isset($_GET["other"]))
    {       
        $pending_match_id = $_GET[MATCH_ID]; 
        $match_info = get_match_info($pending_match_id); 
        $app_id = $match_info[APP_ID];  

        $other_user_id = $_GET["other"]; 
    }

    else
    {
        header("Location: index.php"); 

    }

    $app_info = get_app_info($app_id);
    $user_id = $_COOKIE[USER_ID]; 

    if($app_info == null )
    {
        header("Location: index.php"); 
    }
    
    
    $app_name = $app_info[APP_NAME]; 
   
    // the id of the user who is being searched for 

   if($_SERVER["REQUEST_METHOD"] == "POST")
   {    
     $user_name = $_POST[USER_NAME]; 
     $search_id = user_exist($user_name); 
    

    if($search_id)
    {

        if(already_waiting($search_id,$app_id))
        {
           //is not the same person 
            if(!isset($pending_match_id))
            {
                if($search_id != $user_id && !already_denied($user_id,$search_id,$app_id))
                {   
                   $pending_match_id = add_pending_match($user_id,$search_id,$app_id);
                   update_user_response($user_id,$pending_match_id,$app_id); 
                   send_match_found_mail($pending_match_id,$search_id,$app_id); 
                   header("Location: waiting.php"); 
                }   
                else
                {
                    $error_message = "You are not able to share".$app_name." with ".$user_name; 
                }
            }

            else
            {
                // send an email to declined user  
                // remove users from pending matches table  
                send_match_declined_email($user_id,$other_user_id,$app_id); 
                deny_user($pending_match_id);
                $pending_match_id = add_pending_match($user_id,$search_id,$app_id);
                update_user_response($user_id,$pending_match_id,$app_id); 
                send_match_found_mail($pending_match_id,$search_id,$app_id); 
                header("Location: waiting.php"); 

            }
          

        }   

        elseif(already_matched($search_id,$app_id))
        {
            $error_message = $user_name. " has already been matched for a ".$app_name." account"; 
        }

        else
        {   
            // the user has not paid for the application 
            // in question  
            $error_message = $user_name. " has not paid for a ".$app_name." account"; 

        }


    }   
    else
    {
       $error_message =  "The user you searched for was not found"; 
    }
   }
?>



  <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        

        <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/search_user.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

     

    </head>

     
                   <section>
            <div id="contact-form">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        

                        <form  method="post" id="login-form" >

                                           <div id="error-message">
                                <?php 
                                    echo $error_message;
                                ?>
                            </div>            
                         <div class="row control-group login">
                                <div class="form-group col-xs-12 floating-label-form-group controls">
                                    <label>User name or E-mail</label>
                                    <input type="text" name="<?php echo USER_NAME; ?>" placeholder="User name or E-mail"  required >
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>


                             <div class="row login">
                                <div class="form-group col-xs-12">
                                    <button type="submit" id="login-button"class="btn btn-success btn-lg" >Request Sharemate </button>
                                </div>
                            </div>
                           
                           
                        </form>
                    </div>
                </div>
                <?php if(isset($pending_match_id))
                {
                 ?>
                <b style="margin-left:38%;">NOTE: Requesting a sharemate will cancel your   
                                                existing <?php echo $app_name; ?> match</b> 
                <?php 
                }?> 
            </div>

        </section>





        
       

        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script> 

        <!-- Contact Form JavaScript -->
        <script src="js/jqBootstrapValidation.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/freelancer.js"></script>



    </html>








