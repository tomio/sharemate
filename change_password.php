    <?php
        	require_once("db/config.php");
        	require_once("db/connect.php");
            require_once("db/func.php"); 
        	session_start(); 


            $error_message = "" ;
           
            if(!isset($_GET[EMAIL_KEY]))
            {
            	header("Location: login.php"); 
            }

            $key = $_GET[EMAIL_KEY]; 

            if(!email_key_exists($key))
            {
            	header("Location: login.php"); 
            }




        	if($_SERVER["REQUEST_METHOD"] == "POST")
        	{	
        		$password = trim($_POST[USER_PASSWORD]); 
        		$confirm = trim($_POST["confirm"]);

        		if(!(strlen($password) < 6))
        		{
        			if($password == $confirm)
        			{
        				update_password($key,$password);
        				header("Location: login.php");  
        			}	

        			else 
        			{
        				$error_message = "The passwords you entered do not match"; 
        			}
        		} 

        		else 
        		{
        			$error_message = "Please ensure that the password you enter is atleast 6 characters"; 

        		}

        		
            } 	
        		


        ?>



    <html lang="en">

    <head>

        <meta charset="utf-8">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        

        <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/freelancer.css" rel="stylesheet">
        <link href="css/login.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

     

    </head>



        <!-- Contact Section -->
       <section>
                >
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        
                        <form  method="post" id="login-form" >

                            <div id="error-message">
                                <?php 
                                    echo $error_message;
                                ?>
                            </div>
                           

                             <div class="row control-group login">
                                <div class="form-group col-xs-12 floating-label-form-group controls">
                                    <label>Password</label>
                                    <input type="password" name="<?php echo USER_PASSWORD ?>" placeholder="Password"  required>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>

                          
                            <div class="row control-group login">
                                <div class="form-group col-xs-12 floating-label-form-group controls">
                                    
                                    <label>Confirm Password</label>
                                    <input type="password" name="confirm" placeholder="Confirm Password"  required>
                                    <p class="help-block text-danger"></p>
                               
                                </div>
                            </div>

                             <div class="row login">
                                <div class="form-group col-xs-12">
                                    <button type="submit" id="login-button"class="btn btn-success btn-lg" >Change Password  </button>
                                </div>
                            </div>
                            

                        </form>
                    </div>
                </div>


        </section>



        
       

        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script> 

        <!-- Contact Form JavaScript -->
        <script src="js/jqBootstrapValidation.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/freelancer.js"></script>


    </html>
