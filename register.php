<?php
	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("db/func.php"); 
	
    $error_message = ""; 
	if(isset($_COOKIE[USER_ID]))
	{
		header("Location: index.php"); 
	}

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$user_name = trim($_POST[USER_NAME]); 
		$user_email = trim($_POST[USER_EMAIL]); 
		$user_password = trim($_POST[USER_PASSWORD]);

        if(!already_registered($user_name))
        {
            if(!already_registered($user_email))
            {
                $user_id = create_user($user_name,$user_email,password_hash($user_password,PASSWORD_DEFAULT));  

                setcookie(USER_ID,$user_id); 
 
                send_email($user_name." has signed up ","tomiokiji@gmail.com",$user_name." has signed up "); 

                header("Location: index.php");
                
            }

            else
            {
                $error_message ="The email that you provided is already registered"; 

            }
        }

        else
        {
            $error_message ="The screen name that you provided is already registered"; 

        }

		 
	} 
	


?>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/freelancer.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

 

</head>



<section>
            <div class="row">
                <div class=" text-center">
                    <h2>ShareMate</h2>
                    <h4> Register </h4>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    
                    <form  method="post" id="login-form" >
                        <div id="error-message">
                            <?php 
                                echo $error_message;
                            ?>
                        </div>

                     <div class="row control-group login">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Screen Name</label>
                                <input type="text" value="<?php if(isset($user_name)) echo $user_name; ?>"name="<?php echo USER_NAME ?>" placeholder="Screen Name"  required >
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                         <div class="row control-group login">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>E-mail</label>
                                <input type="email" value="<?php if(isset($user_email)) echo $user_email; ?>"name="<?php echo USER_EMAIL ?>" placeholder="Email"  required>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                         <div class="row control-group login">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Password</label>
                                <input type="password" name="<?php echo USER_PASSWORD ?>" placeholder="Password"  required>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                         <div class="row login">
                            <div class="form-group col-xs-12">
                                <button type="submit" id="login-button"class="btn btn-success btn-lg" >Start Sharing!  </button>
                            </div>
                        </div>

                         <div class="row ">
                            <div class="register-link col-xs-12">
                                <a href="login.php">Login </a>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
    </section>


	<script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script> 

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/freelancer.js"></script>
