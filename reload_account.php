<?php 
	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("db/func.php"); 

	if(!isset($_COOKIE[ADMIN_ID]))
	{
		header("Location: admin_login.php"); 
	}


	if(!isset($_GET[SHARER_ID]) )
	{
		header("Location: matches.php"); 

	}
	$shared_id =$_GET[SHARER_ID]; 
	$info = get_shared_account_info($shared_id); 
	$app = get_app_info($info[APP_ID]); 


	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$status = $_POST["status"]; 
		if(trim($status) == "yes")
		{
			set_reloaded($info[USER_ONE],$info[USER_TWO],$app[APP_ID]); 
			header("Location: matches.php"); 
		}
	}

?>

<link rel="stylesheet" type="text/css" href="css/create_sharer.css">
<div class="user-info">
	<table> 
		<tr>	
			<th> 
				<label for="app-name"> App Name</label>
			</th>
			<td>	
				<p type="text" id="app-name"> <?php echo $app[APP_NAME];  ?></p> 
			</td>
		</tr>

		<tr>	
			<th> 
				<label for="app-cost">App Cost  </label>
			</th>
			<td>	
				<p type="text" id="app-cost" > <?php echo $app[APP_PRICE];?> </p> 
			</td>
		</tr>

		<tr>	
			<th> 
				<label for="last-four">Last four </label>
			</th>
			<td>	
				<p type="text" id="last-four" > <?php echo $info[LAST_FOUR];?></p> 
			</td>
		</tr>	

		
	</table> 

<div class="user-info">
	<form method="post"> 
		<table> 
			


			<tr>	
				<th> 
					<label for="has">Has the accont been reloaded ? </label>
				</th>
				<td>	
					<input type="text" id="has" name="status">
				</td>
			</tr>	
			
			<td><input type="submit"></td>

			
		</table> 

</form> 


	</div>
