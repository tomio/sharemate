  <?php 
	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("db/func.php");   
	$page = "my_apps.php"; 
	require_once("inc/menu.php");  
	require_once("inc/header.php");	


	$user_id = $_COOKIE[USER_ID]; 
		$apps = get_active_apps($user_id);

	// the apps the user waiting on being matched for 
	$apps_waiting = $apps[0]; 
	// the apps that are waiting on responses 
	$apps_pending = $apps[1]; 

	// the apps that the user has already found a match for 
	$apps_matched = $apps[2]; 

	// get the apps that the user has been matched and has credientials for 

	$apps_shared = $apps[3]; 

	$apps_on_trial = $apps[4]; 


	 
	
	$no_apps = false; 
	if(count($apps_waiting) == 0 && count($apps_pending) == 0 && count($apps_matched) == 0 && count($apps_shared) == 0 && count($apps_on_trial) == 0)
		$no_apps = true; 
   


?>

 <link href="css/apps-list.css" rel="stylesheet">

 <?php if($no_apps) {?>
 <h3 id="no-apps">
 	You are not currently sharing any apps 
 </h3>

 <?php }?>

<?php foreach ($apps_waiting as $app_id )
{	
	$app = get_app_info($app_id); 

	$time_left_in_seconds = get_app_start($user_id,$app_id); 
	if($time_left_in_seconds != "" && $time_left_in_seconds != null)
	{
		$next_day = $time_left_in_seconds + ANOTHER_DAY; 
		$time_left = $next_day - time();
		$time_left_text = gmdate("h:i:s",$time_left);
	}
	?>
	<div class="app-description " id="first">
	<ul class='item-list'>
	  <li class='item'>
	    <div class='item__information'>
	      <div class='item__image'>
	        <img src="img/<?php echo strtolower($app[APP_NAME]); ?>.png">
	      </div>
	      <div class='item__body'>
	        <h2 class='item__title'> Status:</h2>
	        <p class='item__description'>
	          <b> Awaiting potential match 
	          </b>
	        </p>
	       
	        <?php if($time_left_in_seconds){ ?>
	        <p >
	       		Time Left: <b class="time-left" value="<?php echo $time_left; ?>"> </b>
	        </p>
	        <?php }?>
            <a href="<?php echo 'search_user.php?'.APP_ID.'='.$app_id; ?>">
              Search for a sharer
            </a> 

	      </div>
	      <div class='item__price js-item-price'></div>
	    </div>
     
	    </div >
    <?php }?>


<?php foreach ($apps_pending as $app_pending_array )
{	
	 
	$app_id = $app_pending_array[0];
	$response_status = $app_pending_array[1];  
	$app = get_app_info($app_id); 	
 

?>
	<div class="app-description ">
	<ul class='item-list'>
	  <li class='item'>
	    <div class='item__information'>
	      <div class='item__image'>
	        <img src="img/<?php echo strtolower($app[APP_NAME]);?>.png">
	      </div>
	      <div class='item__body'>
	        <h2 class='item__title'> Status:</h2>
	        <p class='item__description'>
	          <b> 
	          	<?php if($response_status == NO_RESPONSE)
	          		{
	          			$pending_match_id = get_pending_match_id($user_id,$app_id); 
	          			?>
	      				Awaiting your response  
	      				<br>
	      				<a href="<?php echo 'pending.php?'.MATCH_ID.'='.$pending_match_id; ?>">
	      					Respond now 
	      				</a> 


	      		<?php }


	      			else if($response_status == RESPONSE_ACCEPTED)
	          		{?>
	          			Awaiting response from ShareMate 
	      				

	      		<?php }?>
	          </b>
	        </p>

	      </div>
	      <div class='item__price js-item-price'></div>
	    </div>
	    </div >

  <?php }?>



<?php foreach ($apps_matched as $app_id )
{	
	$app = get_app_info($app_id); 
?>



<div class="app-description ">
<ul class='item-list'>
  <li class='item'>
    <div class='item__information'>
      <div class='item__image'>
	    <img src="img/<?php echo strtolower($app[APP_NAME]);?>.png">
      </div>
      <div class='item__body'>
        <h2 class='item__title'> Status:</h2>
        <p class='item__description'>
        	<b>ShareMate found </b> 
        </p>
        <p> 
        	Awaiting account credentials 
        </p>
      </div>
      <div class='item__price js-item-price'></div>
    </div>
    </div>

    <?php }?>




<?php foreach ($apps_shared as $shared_app_array )
{	
	$app_id = $shared_app_array[0]; 
	$shared_id = $shared_app_array[1]; 
	$app = get_app_info($app_id); 
?>

<div class="app-description ">
<ul class='item-list'>
  <li class='item'>
    <div class='item__information'>
      <div class='item__image'>
	    <img src="img/<?php echo strtolower($app[APP_NAME]);?>.png">
      </div>
      <div class='item__body'>
        <h2 class='item__title'> Status:</h2>
        <p class='item__description'>
        	<b>ShareMate found </b> 
        </p>
        
  	       <a href="rules.php?<?php echo SHARER_ID.'='.$shared_id; ?>"> Get account credentials  </a> 

        
      </div>
      <div class='item__price js-item-price'></div>
    </div>
    </div>

    <?php }?>


<?php foreach ($apps_on_trial as $trial )
{	
		 
	$app_id = $trial[APP_ID]; 
	$app = get_app_info($app_id); 

?>

<div class="app-description ">
<ul class='item-list'>
  <li class='item'>
    <div class='item__information'>
      <div class='item__image'>
	    <img src="img/<?php echo strtolower($app[APP_NAME]);?>.png">
      </div>
      <div class='item__body'>
        <h2 class='item__title'> Status:</h2>

        <?php if($trial[END_DATE] == null)
        {


          ?>

        <p class='item__description'>
        	<b>You get a free month of <?php echo $app[APP_NAME]; ?>! The account credentials will be sent to you shortly </b> 
        </p>
        <?php } ?>

        <?php if($trial[END_DATE] != NULL)
        	 {
        ?>
         
        <p class='item__description'>
        	<b> Free month</b> 
        </p>

  	       <a href="credentials.php?month=true&<?php echo APP_ID.'='.$app_id; ?>"> Get account credentials  </a> 

  	        <?php }?>
        
      </div>
      <div class='item__price js-item-price'></div>
    </div>
    </div>

    <?php }?>










<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

 <script> 
 function startTimer(duration, display) {
    var start = Date.now(),
        diff,
        minutes,
        seconds,
        intervalId; 
    function timer() {
        // get the number of seconds that have elapsed since 
        // startTimer() was called
        diff = duration - (((Date.now() - start) / 1000) | 0);

        // does the same job as parseInt truncates the float
        
        time_difference = Math.round(((diff / 60) | 0) /60) * 60; 
       
        hours = (diff / 3600) | 0;
        

	 	   
        if(time_difference > ((diff / 60) | 0))
        {
            min_diff  =  time_difference - ((diff / 60) | 0)  ; 
            minutes = 60 - min_diff; 
        }

        else
        {
            min_diff  =  ((diff / 60) | 0) - time_difference  ; 
            minutes =  min_diff; 
        }
        


        seconds = (diff % 60) | 0;


       should_free_trial = (hours <= 0 && minutes <= 0 && seconds <= 0); 
        if(!should_free_trial)
        {
        	should_free_trial = hours < 0; 
        	if(!should_free_trial)
	        {
	        	should_free_trial = minutes < 0; 


	        	if(!should_free_trial)
        		{
        			should_free_trial = seconds < 0; 
        		}	

	        }

	     
        
        	
        }










        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

 			
 		if(should_free_trial)
 		{	
 			clearInterval(intervalId);
            display.textContent = "You get a free month! The credentials will be sent to you shortly"; 
        }


       else
         display.textContent = hours +":" +minutes + ":" + seconds; 

        if (diff <= 0) {
            // add one second so that the count down starts at the full duration
            // example 05:00 not 04:59
            start = Date.now() + 1000;
        }
    };
    // we don't want to wait a full second before the timer starts
    timer();
    intervalId= setInterval(timer, 1000);
  
}

window.onload = function () {


	for(i = 0; i < $(".time-left").length; i++)
        {
        	display  = $(".time-left")[i]; 
    	    time = display.getAttribute("value"); 

    		startTimer(time, display);


        }

};

</script>

