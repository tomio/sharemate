<?php 
	

	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("db/func.php");   
	require_once("inc/header.php");


	$user_id = $_COOKIE[USER_ID]; 
	if(isset($_GET[APP_ID]))
	{
		$app_id = $_GET[APP_ID]; 

		$sub_info = get_subbed_app($user_id,$app_id); 
		if(!$sub_info)
			header("Location: index.php"); 
	}

	else
	{
		header("Location: index.php"); 
	}


	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$response = $_POST["user_response"];
		
		if($response == RESPONSE_ACCEPTED)
		{
			cancel_sub($user_id,$app_id);
			header("Location: index.php");  
		}
		else
		{
			header("Location: billing.php"); 
		}

	}



	$app = get_app_info($app_id); 
?>


<link rel="stylesheet" type="text/css" href="css/cancel.css">


<div class="first" id="match-info">

		<form method="post">
		
				<h4 id="accept-text text-center"> Are you sure you wish to cancel you <?php echo $app[APP_NAME]; ?> Subscription ?</h4>
			<div class="container">
				<div class="row buttons">
					<button name="user_response" value="<?php echo RESPONSE_ACCEPTED ; ?>"type="submit" class="col-xs-2 btn btn-lrg btn-success ">Yes </button> 
					<button name="user_response" value="<?php echo RESPONSE_DECLINED; ?>"type="submit" id="decline-button" class="col-xs-2 btn btn-lrg btn-danger ">No </button> 
				</div>
			
				

			</div>


		</form> 

	</div>
