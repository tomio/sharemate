<?php 

	require_once("db/config.php");
	require_once("db/connect.php");
	$no_sign_in = true; 
	require_once("inc/header.php"); 

?>


<link rel="stylesheet" type="text/css" href="css/faq.css">

<div class="message"> 

<a href="#1"> What is sharemate?</a> 
<br> 
<a href="#2"> Is Sharemate Legal?</a> 
<br>
<a href="#3">Can I share with people that I know?</a>
<br> 
<a href="#4"> How can I ensure that my Sharemate will abide by the sharing rules?</a> <br> 
<br>
<p> 
								
	<b>
		<h3 id="1"> What is Sharemate? </h3> 

		Sharemate is a website where you can find people to share your digital applications 
		 with and pay half the price that you would normally pay.  
	</b>
</p>
<br> 


<p> 

	<b>
		<h3 id="2"> Is Sharemate legal? </h3> 

		Yes. Nothing in the terms and conditions of the applications listed on the homepage intimates that it is illegal 
		to share passwords. In fact, these applications encourage for multiple users to be on the same account. 

		
	</b>
</p>

<br>
<p> 

	<b>
		<h3 id="3">Can I share with people that I know?</h3> 

		Yes. Once you pay the sharemate price for the application that you want to share, you 
		have the option to search for other people to share with. 

		
	</b>
</p>

<br> 
<p> 

	<b>
		<h3 id="4"> How can I ensure that my Sharemate will abide by the sharing rules? </h3> 
		We take all cases of misuse very seriously and look into anything that is reported to us. 
		If you feel that your Sharemate has not shared properly, please contact us 
		<a href="contact.php"> here </a>
	
	</b>
</p>



 
</div> 