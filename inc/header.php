<?php   
if(isset($page))
{   
   session_start(); 
   $_SESSION[REQUESTED_PAGE] = $page; 
    
}

if(isset($_COOKIE[USER_ID]))
{   

        $text ="Sign Out"; 
        $link = "sign_out.php"; 
}

else 
{
    $text ="Register"; 
    $link = "register.php"; 

}


if(!isset($no_sign_in) && !isset($_COOKIE[USER_ID]))
	{   

		header("Location: register.php");

	}




?>  




<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/header.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

 

</head>
    <!-- Navigation -->
    <nav class="boarder-box navbar navbar-default navbar-fixed-top">

            <div class="navbar-header page-scroll">
                
                <a class="navbar-brand" href="index.php">ShareMate</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                   
                    <li>
                        <a href="my_apps.php">Account</a>
                    </li>
                     <li>
                        <a href="contact.php">Contact</a>
                    </li>
                     <li>
                        <a href="faq.php">FAQ</a>
                    </li>
                    <li >
                        <a href="<?php echo $link; ?>"><?php echo $text; ?></a>
                    </li>

                </ul>
            <!-- /.navbar-collapse -->
        </div>
    </nav>

