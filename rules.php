<?php 
	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("db/func.php");

	if(isset($_GET[SHARER_ID]))
	{	
		$user_id = $_COOKIE[USER_ID]; 
		$sharer_id = $_GET[SHARER_ID]; 


	}
	else
	{
		header("Location: my_apps.php"); 
	}



	$info = get_shared_account_info($sharer_id); 
	if($info == null)
		header("Location: my_apps.php"); 
	if($user_id != $info[USER_ONE] && $user_id != $info[USER_TWO])
	{
		header("Location: my_apps.php"); 

	}

	$page="rules.php?".SHARER_ID."=".$sharer_id; 
	require_once("inc/header.php"); 

	$app_name = get_app_info($info[APP_ID])[APP_NAME]; 




?>
<link rel="stylesheet" type="text/css" href="css/rules.css">
	<div id="rules"> 
		In order to create a positve experience for all sharers 
		please ensure

		<ul id="rules-list">
			<b>
			<li> That you are the only one using your <?php echo $app_name; ?> account</li>
			<li> That you do not change any of the account information, including the password</li>
			</b>
		</ul>
		<p> 
			<b>
				Violation of any of these conditions 
				will lead to the termination of your account 
			</b>
		</p>

		<p>
		
		 	If you feel that your sharemate has 
		 	failed to meet any of these conditions
		 	<br>
		 	 please contact us 
		 	<a href="contact.php"> here </a>
		
		</p>

		<p>
			<a href="credentials.php?<?php echo SHARER_ID.'='.$sharer_id; ?>"> Continue to account credentials  </a> 

		</p>
	</div>