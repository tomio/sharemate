<?php 
	require_once("db/config.php");
	
if (isset($_COOKIE[USER_ID])) {
	session_start(); 
	$_SESSION = array(); 
	session_destroy(); 
	setcookie(USER_ID, '', time()-300);
     
	header("Location: login.php"); 

}
