<?php 


	require_once("config.php"); 
	require_once("connect.php"); 
	require_once("vendor/autoload.php"); 



function add_app($app_name,$app_price,$app_description)
{
	$con = get_conn(); 

	$create_query = sprintf("INSERT INTO %s(%s,%s,%s) VALUES('%s','%s','%s')",
						APPS_TABLE_NAME,APP_NAME,APP_PRICE,APP_DESCRIPTION,
						mysql_escape_string($app_name),
						mysql_escape_string($app_price),
						mysql_escape_string($app_description)

						); 


	mysqli_query($con,$create_query) or die ( mysqli_error($con)); 
	$insert_id = mysqli_insert_id($con); 
	mysqli_close($con); 

	return $insert_id; 

}

function format_app_name($app_name)
{
	
	$app_name_array = explode("_",$app_name);  
	$name_string = ""; 
	foreach($app_name_array as $piece )
	{
		$name_string =$name_string." ".$piece; 
	}

	return $name_string;

}

function get_apps()
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT * FROM %s",
						APPS_TABLE_NAME					
		); 		

	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	
	$apps = array(); 
	while($row = mysqli_fetch_assoc($result))
	{
		array_push($apps,$row); 
	} 

	return $apps; 



}


function get_app_info($app_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT * FROM %s WHERE %s = %s",
						APPS_TABLE_NAME,
						APP_ID,
						$app_id					
		); 		

	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	
	$app = mysqli_fetch_assoc($result); 
	

	return $app; 

}


function add_user_waiting($user_id,$app_id,$denied_matches = "",$readd = false,$already_subbed =false)
{
	$con = get_conn(); 
		
		// make sure that a user is not getting a free month because he denied a user
		if($readd)
			$start_time = null; 
		else 
			$start_time = time();
 		
		
		$create_query = sprintf("INSERT INTO %s(%s,%s,%s,%s) VALUES('%s','%s','%s','%s')",
							WAITING_TABLE_NAME,USER_ID,APP_ID,DENIED_MATCHES,START_TIME,
							$user_id,
							$app_id,
							$denied_matches,
							$start_time
							); 
		if($readd == false && $already_subbed == false)
		{
			add_sub_date($user_id,$app_id,$con); 
		}

	

	mysqli_query($con,$create_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 

}

function add_sub_date($user_id,$app_id,$con = null )
{

		if($con == null)
		{
			$con =get_conn(); 
			$only_query = true;
		}
 		$date = new DateTime("NOW");
 		$sub_date = $date->format("m/d/y");  
	 
		$create_query = sprintf("INSERT INTO %s(%s,%s,%s) VALUES('%s','%s','%s')",
								SUB_DATES,USER_ID,LAST_BILL,APP_ID,
								$user_id,$sub_date,$app_id
							); 

	

	

	mysqli_query($con,$create_query) or die ( mysqli_error($con)); 

	if(isset($only_query))
	{
		mysqli_close($con);
	}

}


function get_subbed_apps($user_id)
{
		$con = get_conn(); 

		$get_query = sprintf("SELECT * FROM %s WHERE %s = %s",
							SUB_DATES,
							USER_ID,
							$user_id					
			); 		


		$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
		mysqli_close($con); 	
		$sub_info = array(); 

		if($result->num_rows >0)
		{
			while($sub = mysqli_fetch_assoc($result))
			{
				array_push($sub_info,$sub);
			}
		}		

		return $sub_info; 
}


function get_subbed_app($user_id,$app_id)
{
		$con = get_conn(); 

		$get_query = sprintf("SELECT * FROM %s WHERE %s = %s AND %s = %s",
							SUB_DATES,
							USER_ID,
							$user_id,
							APP_ID,
							$app_id					
			); 		


		$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
		mysqli_close($con); 	

		if($result->num_rows >0)
		{
			return mysqli_fetch_assoc($result); 
			
		}

		else 
			return false; 		
}




function get_match($app_id)
{
		$con = get_conn(); 

		$get_query = sprintf("SELECT %s FROM %s WHERE %s = %s",
							USER_ID,
							WAITING_TABLE_NAME,
							APP_ID,
							$app_id					
			); 		


		$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
		mysqli_close($con); 	
		$user_ids = array(); 
		if($result->num_rows >0)
			while($user_id = mysqli_fetch_assoc($result)[USER_ID])
			{
				array_push($user_ids,$user_id) ;
			}
		else 
			return false; 
		

		return $user_ids; 

}

function add_pending_match($user_one_id,$user_two_id,$app_id)
{
	$con = get_conn(); 

	

	// get all of the potential matches denied 
	// by user one, so that if this potential match is denied
	// this list can be added to 

	$user_one_denied = get_denied_text($user_one_id,$app_id); 
	$user_two_denied = get_denied_text($user_two_id,$app_id); 
	$start_time = time(); 



	
	$remove_query = sprintf("DELETE FROM %s WHERE %s = '%s' OR %s = '%s' AND  %s = '%s';",
						WAITING_TABLE_NAME,USER_ID,
						$user_one_id,
						USER_ID,
						$user_two_id,
						APP_ID,
						$app_id); 




	

	mysqli_query($con,$remove_query) or die ( mysqli_error($con)); 

	
	$create_query = sprintf("INSERT INTO %s(%s,%s,%s,%s,%s,%s,%s,%s) VALUES('%s','%s','%s','%s','%s','%s','%s','%s')",
						PENDING_MATCHES_TABLE_NAME,USER_ONE,USER_TWO,
						START_TIME,USER_ONE_DENIED_MATCHES,USER_TWO_DENIED_MATCHES,
						USER_ONE_RESPONSE,USER_TWO_RESPONSE,
						APP_ID,
						$user_one_id,
						$user_two_id,
						$start_time,
						$user_one_denied,
						$user_two_denied,
						NO_RESPONSE,
						NO_RESPONSE,
						$app_id);




	mysqli_query($con,$create_query) or die ( mysqli_error($con)); 
	$insert_id = mysqli_insert_id($con); 
	mysqli_close($con); 

	return $insert_id; 

}


/*
*	Get all of the information concerning the pending_match from 
*	the pending_matches table 
*/
function get_match_info($match_id)
{	
	$con = get_conn(); 

	$get_query = sprintf("SELECT * FROM %s WHERE %s = %s",
						PENDING_MATCHES_TABLE_NAME,
						MATCH_ID,
						$match_id					
		); 		


	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	

	$match_info = mysqli_fetch_assoc($result); 

	

	return $match_info; 
}


function get_mate_info($mate_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE %s = %s",
						USER_NAME,
						USERS_TABLE_NAME,
						USER_ID,
						$mate_id					
		); 		

	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	

	$match_info = mysqli_fetch_assoc($result); 

	

	return $match_info; 
}

function create_match($pending_match_id,$user_one_id,$user_two_id,$app_id)
{	


		$con = get_conn();

        $remove_query = sprintf("DELETE FROM %s WHERE %s = %s",
		PENDING_MATCHES_TABLE_NAME,MATCH_ID, 
	    $pending_match_id
		);


		mysqli_query($con,$remove_query) or die ( mysqli_error($con));

        $create_query = sprintf("INSERT INTO %s(%s,%s,%s)
	VALUES('%s','%s','%s')",
	MATCHES_TABLE_NAME,USER_ONE,USER_TWO,APP_ID,
	$user_one_id,                         $user_two_id,
	$app_id                         );

		mysqli_query($con,$create_query) or die ( mysqli_error($con));
		$insert_id = mysqli_insert_id($con);  mysqli_close($con);

	return $insert_id;



}


function create_user($user_name,$user_email,$user_password)

{
	$con = get_conn(); 

	$date = new DateTime("NOW");
 	$start_date = $date->format("m/y");
	

		$create_query = sprintf("INSERT INTO %s(%s,%s,%s,%s) VALUES('%s','%s','%s','%s')",
						USERS_TABLE_NAME,USER_NAME,USER_EMAIL,USER_PASSWORD,MEMBER_SINCE,
						mysql_escape_string($user_name),
						mysql_escape_string($user_email),
						mysql_escape_string($user_password),
						$start_date
						); 

		echo $create_query; 

		mysqli_query($con,$create_query) or die ( mysqli_error($con)); 
		$insert_id = mysqli_insert_id($con); 
		mysqli_close($con); 

	return $insert_id; 
}


function login_user($user,$user_password)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s,%s FROM %s WHERE %s = '%s' OR %s = '%s' ",
						USER_ID,
						USER_PASSWORD,
						USERS_TABLE_NAME,
						USER_NAME,mysql_escape_string($user),
						USER_EMAIL,mysql_escape_string($user)					
		); 		

	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	
	mysqli_close($con); 	
	if($result->num_rows > 0)
	{
		$user_info = mysqli_fetch_assoc($result); 
		
		if(password_verify($user_password,$user_info[USER_PASSWORD]))
		{
			return $user_info[USER_ID]; 
		} 
		
	}
	

	return ""; 
}


function get_denied_text($user_id,$app_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE %s = %s AND %s = %s",
						DENIED_MATCHES,
						WAITING_TABLE_NAME,
						USER_ID,
						$user_id,
						APP_ID,
						$app_id					
		); 		

	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	

		$denied_matches_text = mysqli_fetch_assoc($result)[DENIED_MATCHES]; 


	

	return $denied_matches_text; 
}


function get_denied($user_id,$app_id)
{
	$denied_matches_text = get_denied_text($user_id,$app_id); 
	
	$denied_matches = explode(",",$denied_matches_text);  
	return $denied_matches; 

}

// check if the user already denied the potential 
// before or vise versa 
function already_denied($user_id,$match_id,$app_id)
{
	$user_denied = get_denied($user_id,$app_id);
	$match_denied = get_denied($match_id,$app_id);

	foreach($user_denied as $denied)
	{
		if($denied ==$match_id)
			{
				return true; 
			}	
	}  

	foreach($match_denied as $denied)
	{
		if($denied ==$user_id)
			{
				return true;  
			}	
	}  


	return false; 


}

// when a match is denied by a user 
function deny_user($pending_match_id)
{	

$con = get_conn(); 
	
	// extarct the info from the pending match table 
	$get_query = sprintf("SELECT %s,%s,%s,%s,%s FROM %s WHERE %s = '%s'",
						USER_ONE,
						USER_TWO,
						USER_ONE_DENIED_MATCHES,
						USER_TWO_DENIED_MATCHES,
						APP_ID,	
						PENDING_MATCHES_TABLE_NAME,
						MATCH_ID,
						$pending_match_id
											
		); 



	// remove users from pending table 
    $remove_query = sprintf("DELETE FROM %s WHERE %s = %s",
		PENDING_MATCHES_TABLE_NAME,
		MATCH_ID, 
	    $pending_match_id
		);

  


	 

	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_query($con,$remove_query) or die ( mysqli_error($con)); 

 	mysqli_close($con); 

	$users_info	 = mysqli_fetch_assoc($result); 	
	$user_one_id = $users_info[USER_ONE]; 
	$user_two_id = $users_info[USER_TWO];
	$app_id = $users_info[APP_ID];  

	$user_one_denied = $user_info[USER_ONE_DENIED_MATCHES]; 
	$user_two_denied = $user_info[USER_TWO_DENIED_MATCHES]; 

	// ensure that neither will be matched again 
	$user_one_denied  = $user_one_denied.",".$user_two_id; 
	$user_two_denied  = $user_two_denied.",".$user_one_id; 



 	// re-add users to waiting table 
 	add_user_waiting($user_one_id,$app_id,$user_one_denied,true); 
 	add_user_waiting($user_two_id,$app_id,$user_two_denied,true);

		

}

// this function will only be called if the user 
// has accepted the match 
function update_user_response($user_id,$match_id,$app_id)
{


	$con = get_conn(); 

	$get_query = sprintf("SELECT %s,%s,%s,%s FROM %s WHERE %s = '%s'",
						USER_ONE,
						USER_TWO,
						USER_ONE_RESPONSE,
						USER_TWO_RESPONSE,
						PENDING_MATCHES_TABLE_NAME,
						MATCH_ID,	
						$match_id
						); 
	echo $get_query;


	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	$users_info  = mysqli_fetch_assoc($result); 
	

	if($user_id == $users_info[USER_ONE])
	{	
		 
		// update the user one response columb 
		 
		if($users_info[USER_TWO_RESPONSE] == RESPONSE_ACCEPTED)
		{	
			// delete from user pending 
			// create new match 
		 	create_match($match_id,$user_id,$users_info[USER_TWO],$app_id); 
		 	return BOTH_ACCEPTED; 
		}
		else 
		{
			$user_resposne_column = USER_ONE_RESPONSE; 

		}

	}
	else if($user_id == $users_info[USER_TWO])
	{
		if( $users_info[USER_ONE_RESPONSE] == RESPONSE_ACCEPTED)
		{
			// delete from user pending 
			// create new match 
			create_match($match_id,$user_id,$users_info[USER_ONE],$app_id);	
			return BOTH_ACCEPTED; 		
		}

		else 
		{
			$user_resposne_column = USER_TWO_RESPONSE; 

		}

	}


	$alter_query = sprintf("UPDATE %s SET %s =%s WHERE %s = %s;",
							PENDING_MATCHES_TABLE_NAME,
							$user_resposne_column,
							RESPONSE_ACCEPTED,
							MATCH_ID,
							$match_id
							);



	 mysqli_query($con,$alter_query) or die ( mysqli_error($con)); 

	 mysqli_close($con); 

	 return -1; 
	

}	

function send_email($content,$address,$subject)
{	
	
	$mail = new PHPMailer();

	$mail->IsSMTP(); // telling the class to use SMTP

	$mail->Host = "ssl://smtp.gmail.com"; // SMTP server
	//$mail->SMTPDebug = 2;
	//Ask for HTML-friendly debug output
	//$mail->Debugoutput = 'html';
	
	$mail->SMTPAuth   = true;                  // enable SMTP authentication

	$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier

	$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server

	$mail->Port       = 465;                   // set the SMTP port for the GMAIL server

	$mail->Username   = "mysharemate@gmail.com";  // GMAIL username

	$mail->Password   = "Nintendo1";            // GMAIL password

	$mail->SetFrom('mysharemate@gmail.com', 'Sharemate');
 
	$mail->Subject = $subject; 
	$mail->MsgHTML($content);


	$mail->AddAddress($address);

	if(!$mail->Send()); 
}

function send_free_trial_updated_email($user_id,$app_name)
{
	$app_name = $app_name;  
	ob_start(); 
	include("free_trial_updated_email.php"); 
	$content = ob_get_clean(); 
	$email = get_user_email($user_id); 
	$subject = "You free month of ".$app_name. " is official!"; 
	send_email($content,$email,$subject); 
}

function send_user_email($user_id,$subject,$content)
{	
	$user = get_user_info($user_id); 

	$mail = new PHPMailer();

	$mail->IsSMTP(); // telling the class to use SMTP

	$mail->Host = "ssl://smtp.gmail.com"; // SMTP server
	//$mail->SMTPDebug = 2;
	//Ask for HTML-friendly debug output
	//$mail->Debugoutput = 'html';
	
	$mail->SMTPAuth   = true;                  // enable SMTP authentication

	$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier

	$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server

	$mail->Port       = 465;                   // set the SMTP port for the GMAIL server

	$mail->Username   = "mysharemate@gmail.com";  // GMAIL username

	$mail->Password   = "Nintendo1";            // GMAIL password
	$mail->AddReplyTo($user[USER_EMAIL],$user[USER_NAME] );

	$mail->SetFrom($user[USER_EMAIL], $user[USER_NAME]);
 
	$mail->Subject = $subject; 
	$mail->MsgHTML($content);


	$mail->AddAddress("mysharemate@gmail.com");

	if(!$mail->Send()); 
}

function send_match_found_mail($pending_match_id,$user_id,$app_id)
{
	$app_info = get_app_info($app_id);
	$app_name = $app_info[APP_NAME];  
	$match_id = $pending_match_id; 
	ob_start(); 
	include("match_found.php"); 
	$content = ob_get_clean(); 
	$email = get_user_email($user_id); 
	$subject = "We found a ShareMate for you !"; 
	send_email($content,$email,$subject); 


}
function send_match_made_email($user_id,$mate_id,$app_id)
{	
	// get the name of the mate 
	// get the app name 
	$match_info = get_match_made_info($mate_id,$app_id); 
	$mate_name = $match_info[USER_NAME]; 
	$app_name = $match_info[APP_NAME]; 
	ob_start(); 
	include("match_made.php"); 
	$content = ob_get_clean(); 
	$email = get_user_email($user_id); 
	$subject = "You have been successfully matched !"; 
	send_email($content,$email,$subject); 

}

function send_credentials_made_email($shared_id,$user_one_id,$user_two_id,$app_name)
{	
	$app_name = $app_name; 

	ob_start(); 
	include("credentials_made.php"); 
	$content = ob_get_clean(); 
	$user_one_email = get_user_email($user_one_id); 
	$user_two_email = get_user_email($user_two_id); 
	$subject = "You ".$app_name." account credentials have been made!"; 
	send_email($content,$user_one_email,$subject);
	send_email($content,$user_two_email,$subject); 



}



function send_password_recovery_email($user_id,$key)
{	
	$key = $key; 

	ob_start(); 
	include("recovery_email.php"); 
	$content = ob_get_clean(); 
	$subject = "Password Recovery"; 
	$user_email = get_user_email($user_id); 

	send_email($content,$user_email,$subject);



}
function get_user_email($user_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE %s = %s",
						USER_EMAIL,
						USERS_TABLE_NAME,
						USER_ID,
						$user_id					
		); 		

	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	

	return  mysqli_fetch_assoc($result)[USER_EMAIL]; 

}

function get_match_made_info($user_id,$app_id)
{
	$con = get_conn(); 

	$get_name_query = sprintf("SELECT %s FROM %s WHERE %s = '%s'",
						USER_NAME,
						USERS_TABLE_NAME,
						USER_ID,
						$user_id					
		); 		
	
	$get_app_query = sprintf("SELECT %s FROM %s WHERE %s = '%s'",
						APP_NAME,
						APPS_TABLE_NAME,
						APP_ID,
						$app_id					
		); 	

	$get_query = $get_name_query.";".$get_app_query;  	
	echo $get_query; 
	mysqli_multi_query($con,$get_query) or die ( mysqli_error($con)); 

	$info_array = array(); 

	  do 
	  {     
	  		$result = mysqli_store_result($con);	
  		    $row = mysqli_fetch_row($result); 
        	array_push($info_array,$row[0]);
        
            mysqli_free_result($result);
    	
    	} while (mysqli_next_result($con));
	 

	return array(
		USER_NAME => $info_array[0],
		APP_NAME =>  $info_array[1]
		); 	

	mysqli_close($con);
 
}


function send_match_declined_email($user_id,$mate_id,$app_id)
{
	// get the name of the mate 
	// get the app name 
	$match_info = get_match_made_info($user_id,$app_id); 
	$mate_name = $match_info[USER_NAME]; 
	$app_name = $match_info[APP_NAME]; 
	ob_start(); 
	include("match_declined.php"); 
	$content = ob_get_clean(); 
	$email = get_user_email($mate_id); 
	$subject = "The match was unsuccessful "; 
	send_email($content,$email,$subject); 
}


function send_card_declined_email($user_id,$app_id)
{	$app = get_app_info($app_id);
	$app_name = $app[APP_NAME]; 
	$app_id = $app_id ; 
	ob_start();
	include("card_declined.php"); 
	$content = ob_get_clean(); 
	$email = get_user_email($user_id); 
	$subject = "Your card was declined"; 
	send_email($content,$email,$subject); 


}

function send_sub_cancelled_email($user_id,$app_id)
{	
	$app = get_app_info($app_id);
	$app_name = $app[APP_NAME]; 
	$app_id = $app_id ; 
	ob_start();	
	include("subscription_cancelled.php"); 
	$content = ob_get_clean(); 
	$email = get_user_email($user_id); 
	$subject = "Your ".$app_name. " subscription was cancelled"; 
	send_email($content,$email,$subject); 	


}


function get_active_apps($user_id)
{
	// get all of the apps the user is waiting to matched 
	$con = get_conn(); 

	$get_apps_waiting_query =  sprintf("SELECT %s FROM %s WHERE %s = '%s'; ",
							APP_ID,
						WAITING_TABLE_NAME,
						USER_ID,
						$user_id					
		); 		
	

	// get all of the the apps the user is waiting on a response for 

	$get_apps_pending_query =  sprintf("SELECT %s,%s,%s,%s FROM %s WHERE %s = '%s' OR %s = '%s';",
						APP_ID,
						USER_ONE,
						USER_ONE_RESPONSE,
						USER_TWO_RESPONSE,
						PENDING_MATCHES_TABLE_NAME,
						USER_ONE,
						$user_id,
						USER_TWO,
						$user_id 					
		); 	
	// get all of the apps the user is matched with and give account credentials 
	 

	$get_apps_matched_query = sprintf("SELECT %s FROM %s WHERE %s = '%s' OR %s = '%s'",
						APP_ID,
						MATCHES_TABLE_NAME,
						USER_ONE,
						$user_id,
						USER_TWO, 
						$user_id					
		); 	


	$get_apps_shared_query = sprintf("SELECT %s,%s FROM %s WHERE %s = '%s' OR %s = '%s'",
						SHARER_ID,APP_ID,
						SHARERS_TABLE_NAME,
						USER_ONE,
						$user_id,
						USER_TWO,
						$user_id
		); 	

	$get_apps_on_free_trial_query =  sprintf("SELECT %s,%s FROM %s WHERE %s = '%s'; ",
						APP_ID,END_DATE,
						FREE_TRIALS,
						USER_ID,
						$user_id					
		); 		







	$apps_waiting = array(); 
	$apps_pending = array(); 
	$apps_matched = array(); 
	$apps_shared = array(); 
	$apps_on_free_trial = array(); 

	$result = mysqli_query($con,$get_apps_waiting_query) or die ( mysqli_error($con)); 
	if($result->num_rows > 0)
	{
		while($row = mysqli_fetch_row($result))
		{
			array_push($apps_waiting,$row[0]); 
		}
	}	

	$result = mysqli_query($con,$get_apps_pending_query) or die ( mysqli_error($con)); 
	if($result->num_rows > 0)
	{
		while($row = mysqli_fetch_assoc($result))
		{	

			// if the user is in the suer one column 
			if($user_id == $row[USER_ONE])
			{

				if($row[USER_ONE_RESPONSE] == RESPONSE_ACCEPTED)
				{
					$status  = RESPONSE_ACCEPTED; 
				}

				else
				{
					$status = NO_RESPONSE; 
				}
			}

			else 
			{

				if($row[USER_TWO_RESPONSE] == RESPONSE_ACCEPTED)
				{
					$status  = RESPONSE_ACCEPTED; 
				}

				else
				{
					$status = NO_RESPONSE; 
				}

			}


			$pending_app_array = array($row[APP_ID],$status); 
			array_push($apps_pending,$pending_app_array);


		}
	}	

	$result = mysqli_query($con,$get_apps_matched_query) or die ( mysqli_error($con)); 
	if($result->num_rows > 0)
	{
		while($row = mysqli_fetch_row($result))
		{
			array_push($apps_matched,$row[0]); 
		}
	}	

	$result = mysqli_query($con,$get_apps_shared_query) or die ( mysqli_error($con)); 
	if($result->num_rows > 0)
	{
		while($row = mysqli_fetch_assoc($result))
		{
			$shared_array = array(); 
			
			array_push($shared_array,$row[APP_ID]); 
			array_push($shared_array,$row[SHARER_ID]);

			array_push($apps_shared,$shared_array); 

		}
	}	


	$result = mysqli_query($con,$get_apps_on_free_trial_query) or die ( mysqli_error($con)); 
	
	if($result->num_rows > 0)
	{
		while($row = mysqli_fetch_assoc($result))
		{
			$trial = array(
				APP_ID =>$row[APP_ID],
				END_DATE=> $row[END_DATE]
				); 
			
			

			array_push($apps_on_free_trial,$trial); 

		}
	}	









	mysqli_close($con); 	
	return  array($apps_waiting,$apps_pending,$apps_matched,$apps_shared,$apps_on_free_trial); 


}


function get_app_start($user_id,$app_id)
{	

	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE %s = '%s' AND %s = '%s'",
						 START_TIME,
						 WAITING_TABLE_NAME,
						 USER_ID,
						 $user_id,
						 APP_ID,
						 $app_id	
						 ); 	

	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	
	$start_time =  mysqli_fetch_row($result)[0]; 

	return $start_time;


}

function get_pending_match_id($user_id,$app_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE %s = '%s' OR %s = '%s' AND %s = '%s'",
						 MATCH_ID,
						 PENDING_MATCHES_TABLE_NAME,
						 USER_ONE,
						 $user_id,
						 USER_TWO,
						 $user_id,
						 APP_ID,
						 $app_id	
						 ); 	

	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	
	$match_id =  mysqli_fetch_row($result)[0]; 

	return $match_id;

}


function already_purchased($user_id,$app_id)
{
	// check the user waiting table 

	$con = get_conn(); 

	$get_waiting_query = sprintf("SELECT %s FROM %s WHERE %s = '%s' AND %s = '%s'",
						 USER_ID,
						 WAITING_TABLE_NAME,
						 USER_ID,
						 $user_id,
						 APP_ID,
						 $app_id

						 ); 	

	$result = mysqli_query($con,$get_waiting_query) or die ( mysqli_error($con)); 

	if($result->num_rows > 0)
	{
		mysqli_close($con); 	
		return true; 


	}	


	// check the pending match table 


	$get_pending_query = sprintf("SELECT %s FROM %s WHERE (%s = '%s' OR  %s = '%s') AND %s = '%s'",
						 USER_ONE,
						 PENDING_MATCHES_TABLE_NAME,
						 USER_ONE,
						 $user_id,
						 USER_TWO,
						 $user_id,
						 APP_ID,
						 $app_id

						 ); 	


		echo $get_pending_query; 	


	$result = mysqli_query($con,$get_pending_query) or die ( mysqli_error($con)); 

	if($result->num_rows > 0)
	{
		mysqli_close($con); 	
		return true; 


	} 	



	// check the matches table


	$get_matched_query = sprintf("SELECT %s FROM %s WHERE (%s = '%s' OR %s = '%s') AND %s = '%s'",
						 USER_ONE,
						 MATCHES_TABLE_NAME,
						 USER_ONE,
						 $user_id,
						 USER_TWO,
						 $user_id,
						 APP_ID,
						 $app_id

						 ); 


	$result = mysqli_query($con,$get_matched_query) or die ( mysqli_error($con)); 

	if($result->num_rows > 0)
	{
		mysqli_close($con); 	
		return true; 


	}

	$get_shared_query = sprintf("SELECT %s FROM %s WHERE (%s = '%s' OR %s = '%s') AND %s = '%s'",
						 SHARER_ID,
						 SHARERS_TABLE_NAME,
						 USER_ONE,
						 $user_id,
						 USER_TWO,
						 $user_id,
						 APP_ID,
						 $app_id

						 ); 


	$result = mysqli_query($con,$get_shared_query) or die ( mysqli_error($con)); 

	if($result->num_rows > 0)
	{
		mysqli_close($con); 	
		return true; 


	}
	


	mysqli_close($con); 	
	return false; 
}


function create_sharer($user_one_id,$user_two_id,$app_id,$shared_email,$shared_password,$last_four,$match_id,$email_password)
{	
	$con = get_conn(); 


	$remove_query = sprintf("DELETE FROM %s WHERE %s = '%s'",
							MATCHES_TABLE_NAME,MATCH_ID,$match_id); 

	$create_query = sprintf("INSERT INTO %s(%s,%s,%s,%s,%s,%s,%s) VALUES('%s','%s','%s','%s','%s','%s','%s')",
						SHARERS_TABLE_NAME,
						USER_ONE,USER_TWO,
						APP_ID,SHARED_EMAIL,
						SHARED_PASSWORD,LAST_FOUR,
						EMAIL_PASSWORD,
						$user_one_id,$user_two_id,
						$app_id,
						mysql_escape_string($shared_email),
						mysql_escape_string($shared_password),
						$last_four,
						$email_password

						); 

	$increment_query = $alter_query = sprintf("UPDATE %s SET %s =%s +1 WHERE %s = '%s' OR %s = '%s';",
							USERS_TABLE_NAME,
							AMOUNT_SHARED,
							AMOUNT_SHARED,
							USER_ID,
							$user_one_id,
							USER_ID,
							$user_two_id
							);

	mysqli_query($con,$increment_query) or die ( mysqli_error($con)); 


	update_sub($user_one_id,$app_id,$con);
	update_sub($user_two_id,$app_id,$con); 

	mysqli_query($con,$remove_query) or die ( mysqli_error($con)); 
	mysqli_query($con,$create_query) or die ( mysqli_error($con)); 
	$insert_id = mysqli_insert_id ($con); 
	mysqli_close($con); 

	return $insert_id;

}


function update_sub($user_id,$app_id,$con)
{

	$date = new DateTime("NOW");
 		$sub_date = $date->format("m/d/y"); 

	$date = new DateTime("NOW");
	$sub_date = $date->format("m/d/y"); 
	$date->modify("+26 day");
	$next_bill_date = $date->format("m/d/y"); 



	$alter_query = sprintf("UPDATE %s SET %s ='%s',%s='%s' WHERE %s = '%s' AND %s = '%s';",
							SUB_DATES,
							SUB_DATE,
							$sub_date,
							NEXT_BILL,
							$next_bill_date,
							USER_ID,
							$user_id,
							APP_ID,
							$app_id
							);



	mysqli_query($con,$alter_query) or die ( mysqli_error($con)); 




		
}


function get_matches()
{	
	$con = get_conn(); 
	$matches = array(); 

	$get_matched_query = sprintf("SELECT * FROM %s",
								MATCHES_TABLE_NAME); 


	$result = mysqli_query($con,$get_matched_query) or die ( mysqli_error($con)); 

	if($result->num_rows > 0)
	{	
		while($row = mysqli_fetch_assoc($result))
		{
			array_push($matches,$row); 
		}



	}
	
	return $matches; 


	mysqli_close($con); 	


}

function create_admin($name,$password)
{

	$con = get_conn(); 

	$create_query = sprintf("INSERT INTO %s(%s,%s) VALUES('%s','%s')",
						ADMIN_TABLE_NAME,
						ADMIN_NAME,
						ADMIN_PASSWORD,
						mysql_escape_string($name),
						mysql_escape_string(password_hash($password,PASSWORD_DEFAULT))
						); 


	mysqli_query($con,$create_query) or die ( mysqli_error($con)); 
	$insert_id = mysqli_insert_id ($con); 
	mysqli_close($con); 
 
}

function login_admin($name,$password)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s,%s FROM %s WHERE %s = '%s'",
						ADMIN_ID,ADMIN_PASSWORD,ADMIN_TABLE_NAME,ADMIN_NAME,$name				
		); 		


	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	
	if($result->num_rows)
	{
		$row = mysqli_fetch_assoc($result); 
		
		if(password_verify($password,$row[ADMIN_PASSWORD]))
		{
			return $row[ADMIN_ID]; 
		}
		
	}

}

function get_shared_account_info($shared_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT * FROM %s WHERE %s = '%s'",
						SHARERS_TABLE_NAME,SHARER_ID,$shared_id			
		); 		


	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	
	if($result->num_rows)
	{
		return mysqli_fetch_assoc($result); 
		
	
		
	}
}


function get_user_info($user_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT * FROM %s WHERE %s = '%s'",
						USERS_TABLE_NAME,USER_ID,$user_id			
		); 		


	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	
	if($result->num_rows >0)
	{
		return mysqli_fetch_assoc($result); 
		
	}
}

function already_registered($user_email)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE %s = '%s' OR  %s = '%s'",
						USER_ID,USERS_TABLE_NAME,USER_EMAIL,$user_email,
						USER_NAME,$user_email		
		); 		


	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	
	if($result->num_rows > 0)
	{
		return true; 
		
	}

	return false; 
}


function cancel_sub($user_id,$app_id)
{
	$con = get_conn(); 

	$alter_query = sprintf("UPDATE %s SET %s =%s WHERE %s = %s AND %s = %s ;",
							SUB_DATES,
							RECUR,
							NOT_RECUR,
							USER_ID,
							$user_id,
							APP_ID,
							$app_id
							);

	 mysqli_query($con,$alter_query) or die ( mysqli_error($con)); 


	 mysqli_close($con); 	

}

function set_stripe_id($user_id,$app_id,$stripe_id)
{
	$con = get_conn(); 


	$alter_query = sprintf("UPDATE %s SET %s ='%s' WHERE %s = '%s' AND %s = '%s'",
							SUB_DATES,STRIPE_ID,
							$stripe_id,USER_ID,$user_id,
							APP_ID, $app_id
							
			);



	mysqli_query($con,$alter_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	


}


function get_all_bill_dates()
{

	$con = get_conn(); 
	$bill_dates = array(); 
	$get_query = sprintf("SELECT * FROM %s WHERE %s IS NOT NULL ",
						SUB_DATES,NEXT_BILL
		); 		


	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	

	if($result->num_rows > 0)
	{
		while($row = mysqli_fetch_assoc($result))
		{
			array_push($bill_dates,$row); 
		}
			
	}

	return $bill_dates; 
}


function is_today($bill_date)
{
	$date = new DateTime("NOW");
	$today = $date->format("m/d/y");  

	if($bill_date == $today)
		return true; 
	else 
		return false; 

}


function update_bill_date($user_id,$app_id)
{

	$con = get_conn(); 

	$date = new DateTime("NOW"); 
	$today = $date->format("m/d/y");
	$next_bill = $date->modify("+26 day");
	$next_bill_date = $date->format("m/d/y");


	$alter_query = sprintf("UPDATE %s SET %s ='%s',%s ='%s' WHERE %s = '%s' AND %s = '%s'",
							SUB_DATES,LAST_BILL,
							$today,NEXT_BILL,
							$next_bill_date,
							USER_ID,
							$user_id,
							APP_ID,
							$app_id
							
			);



	mysqli_query($con,$alter_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 

}
function get_sharemate_id($user_id,$app_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s,%s FROM %s WHERE (%s = %s OR %s = %s) AND %s = %s",
							USER_ONE,USER_TWO,SHARERS_TABLE_NAME,
							USER_ONE,$user_id,
							USER_TWO,$user_id,
							APP_ID,$app_id
		); 		

	
	
	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 
	if(!($result->num_rows > 0) )
		return false ; 

	$user_ids = mysqli_fetch_assoc($result ); 

	if($user_id == $user_ids[USER_ONE])
	{
		$mate_id = $user_ids[USER_TWO]; 
	}

	else 
	{
		$mate_id = $user_ids[USER_ONE]; 

	}


 	return $mate_id; 


 
}

function get_next_bill($user_id,$app_id)
{

	$con = get_conn(); 


$get_sub_query = sprintf("SELECT %s FROM %s WHERE  %s = %s  AND %s = %s",
							NEXT_BILL,SUB_DATES,
							USER_ID,$user_id,
							APP_ID,$app_id
		); 	

	$result = mysqli_query($con,$get_sub_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 

	$sub_info = mysqli_fetch_assoc($result);

	return $sub_info[NEXT_BILL]; 

}



function get_sharemate_sub_info($user_id,$app_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s,%s FROM %s WHERE (%s = %s OR %s = %s) AND %s = %s",
							USER_ONE,USER_TWO,SHARERS_TABLE_NAME,
							USER_ONE,$user_id,
							USER_TWO,$user_id,
							APP_ID,$app_id
		); 		

	echo "<br>"; 
	echo $get_query; 	
	echo "<br>"; 
	
	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 

	if(!($result->num_rows > 0) )
		return false ; 

	$user_ids = mysqli_fetch_assoc($result ); 

	if($user_id == $user_ids[USER_ONE])
	{
		$mate_id = $user_ids[USER_TWO]; 
	}

	else 
	{
		$mate_id = $user_ids[USER_ONE]; 

	}


	$get_sub_query = sprintf("SELECT %s,%s,%s FROM %s WHERE  %s = %s  AND %s = %s",
							NEXT_BILL,STRIPE_ID,USER_ID,SUB_DATES,
							USER_ID,$mate_id,
							APP_ID,$app_id
		); 	

	


	$result = mysqli_query($con,$get_sub_query) or die ( mysqli_error($con)); 

	mysqli_close($con); 	


	$sub_info = mysqli_fetch_assoc($result);

	if(is_today($sub_info[NEXT_BILL]))
	{
		return $sub_info; 
	}

	else
	{
		return false; 
	}


 
}



function remove_sharers($user_id,$app_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE (%s = '%s' OR %s = '%s') AND  %s = '%s';",
						LAST_FOUR,
						SHARERS_TABLE_NAME,
						USER_ONE,$user_id,
						USER_TWO,$user_id,
						APP_ID,$app_id
		); 


	


	$remove_query = sprintf("DELETE FROM %s WHERE (%s = '%s' OR %s = '%s') AND  %s = '%s';",
						SHARERS_TABLE_NAME,
						USER_ONE,$user_id,
						USER_TWO,$user_id,
						APP_ID,$app_id
						); 	




    $result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
    $last_four = mysqli_fetch_row($result)[0]; 


    // inser the card that should be removed from 
    // the datebase 

    $insert_query = sprintf("INSERT INTO %s(%s) VALUES('%s')",
							TO_BE_CANCELLED,
							LAST_FOUR,
							$last_four); 

    send_email("Another card needs to be cancelled","tomiokiji@gmail.com","A card with the last four:".$last_four." needs to be cancelled"); 

    mysqli_query($con,$insert_query) or die ( mysqli_error($con)); 

    mysqli_query($con,$remove_query) or die ( mysqli_error($con)); 

	mysqli_close($con); 	



}

function remove_sub_date($user_id,$app_id)
{

	$con = get_conn(); 

	$remove_query = sprintf("DELETE FROM %s WHERE %s = '%s' AND  %s = '%s';",
						SUB_DATES,
						APP_ID,$app_id,
						USER_ID,$user_id
						);



	mysqli_query($con,$remove_query) or die ( mysqli_error($con)); 

	mysqli_close($con); 	
}

function set_next_bill_null($user_id,$app_id)
{
	$con = get_conn(); 

	$alter_query = sprintf("UPDATE %s SET %s = NULL,%s = NULL WHERE %s = %s AND %s = %s;",
							SUB_DATES,
							NEXT_BILL,
							SUB_DATE,
							APP_ID,
							$app_id,
							USER_ID,
							$user_id
							);
	mysqli_query($con,$alter_query) or die ( mysqli_error($con)); 

	mysqli_close($con); 	
}


// check if the next bill date can be determined 
// and that the user is still suscribed

function next_bill_not_null($user_id,$app_id)
{
		$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE  %s = '%s'  AND %s = '%s'",
							NEXT_BILL,SUB_DATES,
							USER_ID,$user_id,
							APP_ID,$app_id
		); 	

	
	echo $get_query; 

	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 


	mysqli_close($con); 

	if($result->num_rows > 0)
	{
		$next_bill = mysqli_fetch_row($result)[0]; 

		if($next_bill == null)
		{
			return false;
		}	
		else 
		{
			return true; 
		}

	}

	else
		return false; 
}

function cancelled_sub($user_id,$app_id)
{

	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE  %s = %s  AND %s = %s",
							RECUR,SUB_DATES,
							USER_ID,$user_id,
							APP_ID,$app_id
		); 	

	


	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 


	mysqli_close($con); 

	if($result->num_rows > 0)
	{
		$recur_status = mysqli_fetch_row($result)[0]; 

		if($recur_status == WILL_RECUR)
		{
			return false;
		}	
		else 
		{
			return true; 
		}

	}

	else
		return false; 
}

// check if the sharemate of the user has cancelled the subscription 




function get_sharemate_info($user_id,$app_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s,%s FROM %s WHERE (%s = %s OR %s = %s) AND %s = %s",
							USER_ONE,USER_TWO,SHARERS_TABLE_NAME,
							USER_ONE,$user_id,
							USER_TWO,$user_id,
							APP_ID,$app_id
		); 		


	
	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	

	$user_ids = mysqli_fetch_assoc($result ); 

	if($user_id == $user_ids[USER_ONE])
	{
		$mate_id = $user_ids[USER_TWO]; 
	}

	else 
	{
		$mate_id = $user_ids[USER_ONE]; 

	}

	return $mate_id; 


}


function user_still_suscribed($user_id,$app_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE %s = %s AND %s = %s",
							USER_ID,SUB_DATES,
							USER_ID,$user_id,
							APP_ID,$app_id
							
		); 		


	
	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	


	if($result->num_rows > 0)
		return true; 
	else 
		return false; 

}


function already_waiting($user_id,$app_id)
{


	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE %s = %s AND %s = %s",
							USER_ID,WAITING_TABLE_NAME,
							USER_ID,$user_id,
							APP_ID,$app_id
							
		); 		


	
	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	


	if($result->num_rows > 0)
		return true; 
	else 
		return false; 

}


function users_awaiting_free_month()
{

	$con = get_conn(); 

	$get_query = sprintf("SELECT * FROM %s WHERE %s IS NULL",
							FREE_TRIALS,
							END_DATE
		); 		


	



	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	

	$users_waiting = array(); 
	

	if($result->num_rows > 0)
	{
		while($row = mysqli_fetch_assoc($result))
		{

			array_push($users_waiting,$row); 

		}
	}	



	return $users_waiting; 
}


function get_users_waiting()
{

	$con = get_conn(); 

	$get_query = sprintf("SELECT * FROM %s",
							WAITING_TABLE_NAME
		); 		


	



	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	

	$users_waiting = array(); 
	

	if($result->num_rows > 0)
	{
		while($row = mysqli_fetch_assoc($result))
		{

			array_push($users_waiting,$row); 

		}
	}	



	return $users_waiting ;

}


function add_free_trial($user_id,$app_id)
{
	$con = get_conn(); 


	// remove the user from the waiting table 

	$remove_query = sprintf("DELETE FROM %s WHERE %s = '%s' AND  %s = '%s';",
						WAITING_TABLE_NAME,
						USER_ID,
						$user_id,
						APP_ID,
						$app_id); 

	mysqli_query($con,$remove_query) or die ( mysqli_error($con)); 

	$create_query = sprintf("INSERT INTO %s(%s,%s) VALUES('%s','%s')",
						FREE_TRIALS,USER_ID,APP_ID,
						$user_id,
						$app_id

						); 


	mysqli_query($con,$create_query) or die ( mysqli_error($con)); 
	$insert_id = mysqli_insert_id($con); 
	mysqli_close($con); 

	return $insert_id; 	
}


function update_free_trial($user_id,$app_id,$account_email,$account_password,$email_password,$end_date,$last_four)
{


	$con = get_conn(); 
	$date = new DateTime("NOW"); 
	$sub_date = $date->format("m/d/y");

	$update_query = sprintf("UPDATE %s SET %s='%s',%s='%s',%s='%s',%s='%s',%s='%s',%s='%s' WHERE  %s=%s AND %s=%s ",
								FREE_TRIALS,
								END_DATE,$end_date,
								LAST_FOUR,$last_four,
								SUB_DATE,$sub_date,
								SHARED_EMAIL,$account_email,
								SHARED_PASSWORD,$account_password,
								EMAIL_PASSWORD,$email_password,
								USER_ID,$user_id,
								APP_ID,	$app_id
								
							); 


	 
	mysqli_query($con,$update_query) or die ( mysqli_error($con));
	mysqli_close($con); 
}




function get_empty_accounts()
{

	$con = get_conn(); 
	$accounts = array(); 
	$date = new DateTime("NOW"); 
	$today = $date->format("m/d/y");   
	$get_query = sprintf("SELECT * FROM %s WHERE %s IS NOT NULL AND %s != '%s' AND %s='%s' ",
						SUB_DATES,
						NEXT_BILL,
						NEXT_BILL,
						$today,
						ACCOUNT_RELOADED,
						NOT_RELOADED 
						); 



	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con));

	if($result->num_rows > 0)
	{
		while($user = mysqli_fetch_assoc($result))
		{	

			$user_id = $user[USER_ID]; 
			$app_id = $user[APP_ID]; 
			$get_sharers_query = sprintf("SELECT *FROM  %s WHERE (%s = %s OR %s = %s) AND %s = %s"
												,SHARERS_TABLE_NAME,
												USER_ONE,$user_id,
												USER_TWO,$user_id,
												APP_ID,$app_id
												

												 ); 


			$sharers_result = mysqli_query($con,$get_sharers_query) or die ( mysqli_error($con));
			$sharers = mysqli_fetch_assoc($sharers_result);

			

			if($user_id == $sharers[USER_ONE])
				$mate_id = $sharers[USER_TWO]; 	
			else
				$mate_id = $sharers[USER_ONE]; 


			$get_mate_sub_query = sprintf("SELECT * FROM %s WHERE %s = '%s' AND  %s IS NOT NULL AND %s != '%s' AND %s = %s",
						SUB_DATES,
						USER_ID,
						$mate_id,
						NEXT_BILL,
						NEXT_BILL,
						$today,
						ACCOUNT_RELOADED,
						NOT_RELOADED
						); 

	
			$mate_sub_result =mysqli_query($con,$get_mate_sub_query) or die ( mysqli_error($con));

			if($mate_sub_result->num_rows > 0)
			{	
  				$key = array_search($sharers[SHARER_ID], $accounts); 

				if($key >= 0)
					unset($accounts[$key]); 
				array_push($accounts,$sharers[SHARER_ID]); 
			}


		}	

	}
	mysqli_close($con); 
	 

	return $accounts;

}


function get_all_to_be_cancelled()
{	
	$con = get_conn(); 
	$cards = array(); 
	
	$get_query = sprintf("SELECT * FROM %s ",
				TO_BE_CANCELLED); 
	
	$result =mysqli_query($con,$get_query) or die ( mysqli_error($con));
	if($result->num_rows > 0)
	{
		while($row = mysqli_fetch_row($result))
		{
			array_push($cards,$row[0]); 
		}
	}


	return $cards; 

	mysqli_close($con); 

}


function notify_reload($user_id,$mate_id,$app_id)
{	
	$con = get_conn(); 
	$update_query = sprintf("UPDATE %s SET %s = %s WHERE (%s=%s AND %s=%s) OR (%s = %s AND %s=%s)",
								SUB_DATES,ACCOUNT_RELOADED,
								NOT_RELOADED,USER_ID,
								$user_id,APP_ID,
								$app_id,
								USER_ID,$mate_id,
								APP_ID,$app_id
							);
 
	

	mysqli_query($con,$update_query) or die ( mysqli_error($con));
	mysqli_close($con); 
	send_email("Another card needs to be reloaded","tomiokiji@gmail.com","Another card needs to be reloaded"); 


}


function set_reloaded($user_id,$mate_id,$app_id)
{
	$con = get_conn(); 
	$update_query = sprintf("UPDATE %s SET %s = %s WHERE (%s=%s AND %s=%s) OR (%s = %s AND %s=%s)",
								SUB_DATES,ACCOUNT_RELOADED,
								RELOADED,USER_ID,
								$user_id,APP_ID,
								$app_id,
								USER_ID,$mate_id,
								APP_ID,$app_id
							); 

	mysqli_query($con,$update_query) or die ( mysqli_error($con));
	mysqli_close($con); 
}


function set_card_cancelled($last_four)
{
	$con = get_conn(); 
	$delete_query = sprintf("DELETE FROM  %s WHERE %s = %s",
								TO_BE_CANCELLED,LAST_FOUR,
								$last_four
							); 

	mysqli_query($con,$delete_query) or die ( mysqli_error($con));
	mysqli_close($con); 
}


function already_matched($user_id,$app_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE ( %s = %s OR %s= %s ) AND %s = %s",
						USER_ONE, 
						PENDING_MATCHES_TABLE_NAME,
						USER_ONE,$user_id,
						USER_TWO,$user_id,
						APP_ID,$app_id

		); 		

	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	
		
	if($result->num_rows > 0)
	{
		return true; 
	}

	else 
		false; 

}


function create_email_key($user_id,$user_email)
{
	$con = get_conn(); 
	$key = md5(microtime().rand());
	$start_time = time(); 


	$create_query = sprintf("INSERT INTO %s(%s,%s,%s) VALUES('%s','%s','%s')",
						FORGOTTEN,USER_ID,EMAIL_KEY,START_TIME,
						$user_id,$key,$start_time

						); 

	echo $create_query; 


	mysqli_query($con,$create_query) or die ( mysqli_error($con)); 
	$insert_id = mysqli_insert_id($con); 

	mysqli_close($con); 
	return $key; 


}




	function get_user_by_email($user_email)
	{
		$con = get_conn(); 

		$get_query = sprintf("SELECT %s FROM %s WHERE %s = '%s'",
							USER_ID,
							USERS_TABLE_NAME,
							USER_EMAIL,
							$user_email					
			); 		

		$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
		mysqli_close($con); 	
			
		if($result->num_rows > 0)
		{
		$user_id = mysqli_fetch_row($result)[0]; 
		return $user_id; 
	}

	else
	{
		return false; 
	}



} 


function update_password($key,$password)
{	
	$password = password_hash($password,PASSWORD_DEFAULT); 
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE %s = '%s'",
						USER_ID,FORGOTTEN,
						EMAIL_KEY,$key					
		); 		

	$remove_query = sprintf("DELETE FROM %s WHERE %s= '%s'",
								FORGOTTEN,EMAIL_KEY,
								$key
							); 



	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con));
	mysqli_query($con,$remove_query) or die ( mysqli_error($con)); 
	$user_id = mysqli_fetch_row($result)[0]; 
	
	$update_query  = sprintf("UPDATE %s SET %s ='%s' WHERE %s = '%s';",
								USERS_TABLE_NAME,
								USER_PASSWORD,
								$password,
								USER_ID,$user_id
		); 


	mysqli_query($con,$update_query) or die ( mysqli_error($con));



	mysqli_close($con); 	


}

function email_key_exists($key)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE %s = '%s'",
						USER_ID,FORGOTTEN,
						EMAIL_KEY,$key					
		); 		

	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con));
	mysqli_close($con);

	if($result->num_rows > 0)
	{
		return true; 
	}
	else 
		return false; 
}

function get_all_email_keys()
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT * FROM %s ",
						FORGOTTEN		
						); 		



	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con));


	mysqli_close($con); 	
 	$keys = array(); 

 	if($result->num_rows > 0 )
 	{	
 		 while($row = mysqli_fetch_assoc($result))
 		 {
 		 	array_push($keys,$row); 
 		 }
 	}

 	return $keys; 
}


function remove_email_key($key)
{
	$con = get_conn(); 

	$remove_query = sprintf("DELETE FROM %s WHERE %s= '%s'",
								FORGOTTEN,EMAIL_KEY,
								$key
							); 

	mysqli_query($con,$remove_query) or die ( mysqli_error($con));


	mysqli_close($con); 

}

function get_free_month_credentials($user_id,$app_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s,%s FROM %s WHERE %s = '%s'",
						SHARED_EMAIL,SHARED_PASSWORD,
						FREE_TRIALS,
						APP_ID,$app_id,
						USER_ID,$user_id		
		); 		


	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	
	if($result->num_rows)
	{
		return mysqli_fetch_assoc($result); 
		
	
		
	}
}

function set_stripe_charge($user_id,$app_id,$stripe_charge)
{
	$con = get_conn(); 

	$update_query = sprintf("UPDATE %s SET %s='%s' WHERE %s= %s AND %s=%s",
									WAITING_TABLE_NAME,
									STRIPE_CHARGE,
									$stripe_charge,
									USER_ID,$user_id,
									APP_ID,$app_id
							); 
	

	mysqli_query($con,$update_query) or die ( mysqli_error($con)); 

	mysqli_close($con); 
}

function get_stripe_charge($user_id,$app_id)
{
	$con = get_conn(); 

	$get_query = sprintf("SELECT %s FROM %s WHERE %s = %s AND %s = %s",
						STRIPE_CHARGE,
						WAITING_TABLE_NAME,
						USER_ID,
						$user_id,
						APP_ID,
						$app_id					
		); 		


	$result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
	mysqli_close($con); 	

	if($result->num_rows >0)
	{
		return mysqli_fetch_assoc($result)[STRIPE_CHARGE]; 
		
	}

	else 
		return false; 		
}
	
 function user_exist($user)
{
    $con = get_conn(); 

    $get_query = sprintf("SELECT %s FROM %s WHERE %s = '%s' OR %s = '%s' ",
                        USER_ID,
                        USERS_TABLE_NAME,
                        USER_NAME,mysql_escape_string($user),
                        USER_EMAIL,mysql_escape_string($user)                   
        );      

    $result = mysqli_query($con,$get_query) or die ( mysqli_error($con)); 
    
    mysqli_close($con); 

    if($result->num_rows > 0)
    {
        return mysqli_fetch_row($result)[0]; 
    }
    else
    {
        return false;
    }
}

function remove_user_waiting($user_id,$app_id)
{

	$con = get_conn(); 
	$remove_query = sprintf("DELETE FROM %s WHERE %s = '%s' AND  %s = '%s';",
						WAITING_TABLE_NAME,USER_ID,
						$user_id,
						APP_ID,
						$app_id); 




	mysqli_query($con,$remove_query) or die ( mysqli_error($con)); 

	mysqli_close($con); 
}