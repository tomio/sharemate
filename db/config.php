<?php 


	DEFINE("DB_HOST","localhost"); 
	DEFINE("DB_USERNAME","root"); 
	DEFINE("DB_PASSWORD","nintendo"); 
	DEFINE("DB_NAME","sharemate");

	DEFINE("APPS_TABLE_NAME","apps"); 
	DEFINE("APP_ID","app_id"); 

	DEFINE("USERS_TABLE_NAME","users");
	DEFINE("USER_ID","user_id"); 
	DEFINE("USER_NAME","user_name");
	DEFINE("USER_EMAIL","user_email");
	DEFINE("USER_PASSWORD","user_password"); 
	DEFINE("MEMBER_SINCE","member_since");
	DEFINE("AMOUNT_SHARED","amount_shared");  


	DEFINE("APP_NAME","app_name"); 
	DEFINE("APP_PRICE","app_price"); 
	DEFINE("APP_DESCRIPTION","app_description");

	DEFINE("WAITING_TABLE_NAME","waiting_match"); 

	DEFINE("PENDING_MATCHES_TABLE_NAME","pending_matches"); 
	DEFINE("MATCH_ID","match_id"); 
	DEFINE("USER_ONE","user_one");
	DEFINE("USER_TWO","user_two"); 
	DEFINE("START_TIME","start_time"); 
	DEFINE("USER_ONE_RESPONSE","user_one_response");
	DEFINE("USER_TWO_RESPONSE","user_two_response");
	DEFINE("NO_RESPONSE","0");
	
	DEFINE("RESPONSE_ACCEPTED","1");  
	DEFINE("RESPONSE_DECLINED","2");  
	DEFINE("BOTH_ACCEPTED","5"); 

	DEFINE("USER_ONE_DENIED_MATCHES","user_one_denied_matches");
	DEFINE("USER_TWO_DENIED_MATCHES","user_two_denied_matches"); 
	

	 DEFINE("MATCHES_TABLE_NAME","matches"); 
	 DEFINE("DENIED_MATCHES","denied_matches"); 
	 DEFINE("SITE_URL","https://sharemate.me/"); 


	 DEFINE("PRIVATE_KEY","sk_live_d0mYCw2Q2KdI0tRMgOQLYFaD"); 
	 DEFINE("STRIPE_KEY","pk_live_BZsprNGlg7Jnkv7eFPNxF6Lj"); 
 
	 DEFINE("REQUESTED_PAGE","requested_page"); 
	 DEFINE("ANOTHER_DAY","86400"); 

	 DEFINE("SHARERS_TABLE_NAME","sharers"); 
	 DEFINE("SHARER_ID","share_id"); 
	 DEFINE("SUB_DATE","subscription_date");
	 DEFINE("SHARED_EMAIL","email"); 
	 DEFINE("SHARED_PASSWORD","password");   
	 DEFINE("NEXT_BILL","next_bill_date"); 
	 DEFINE("LAST_BILL","last_bill_date"); 
	 DEFINE("LAST_FOUR","last_four"); 

	 DEFINE("ADMIN_TABLE_NAME","admin"); 
	 DEFINE("ADMIN_ID","admin_id");
	 DEFINE("ADMIN_NAME","admin_name");
	 DEFINE("ADMIN_PASSWORD","admin_password");   
	 DEFINE("SUB_DATES","subscription_dates"); 
	 DEFINE("ACCOUNT_RELOADED","account_reloaded"); 
	 DEFINE("RELOADED","1"); 
	 DEFINE("NOT_RELOADED","0"); 

	 DEFINE("RECUR","recur");
	 DEFINE("WILL_RECUR","1");
	 DEFINE("NOT_RECUR","0"); 

	 DEFINE("STRIPE_ID","stripe_id");   

	 DEFINE("EMAIL_PASSWORD","email_password"); 

	 DEFINE("FREE_TRIALS","free_trials"); 

	 DEFINE("TO_BE_CANCELLED","cards_to_be_cancelled");
	 DEFINE("FORGOTTEN","forgotten_passwords"); 
	 DEFINE("EMAIL_KEY","email_key");
	 DEFINE("STRIPE_CHARGE","stripe_charge"); 
	 DEFINE("FREQUENCY","frequency"); 
	 DEFINE("END_DATE","end_date"); 