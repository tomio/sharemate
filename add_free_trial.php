<?php 
require_once("db/config.php");
	require_once("db/connect.php");
	require_once("db/func.php"); 

	

	if(!isset($_COOKIE[ADMIN_ID]))
	{
		header("Location: admin_login.php"); 
	}

	if(!isset($_GET[USER_ID]) || !isset($_GET[APP_ID]))
	{
		header("Location: matches.php"); 

	}


	$user_id = $_GET[USER_ID] ; 
	$app_id = $_GET[APP_ID];

	$user = get_user_info($user_id); 
	$app = get_app_info($app_id); 

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$account_email = $_POST[SHARED_EMAIL]; 
		$account_password = $_POST[SHARED_PASSWORD]; 
		$email_password = $_POST[EMAIL_PASSWORD]; 
		$last_four =$_POST[LAST_FOUR]; 
		$end_date = $_POST[END_DATE]; 

		update_free_trial($user_id,$app_id,$account_email,$account_password,$email_password,$end_date,$last_four); 
		send_free_trial_updated_email($user_id,$app_name); 
		header("Location: matches.php"); 




	}

?>


<link rel="stylesheet" type="text/css" href="css/create_sharer.css">
<div class="user-info">
	<table> 
		<tr>	
			<th> 
				<label for="user_one">User One Name</label>
			</th>
			<td>	
				<p type="text" id="password"> <?php echo $user[USER_NAME];  ?></p> 
			</td>
		</tr>

		<tr>	
			<th> 
				<label for="app_name">Application Name </label>
			</th>
			<td>	
				<p type="text" id="app_name" > <?php echo $app[APP_NAME];?></p> 
			</td>
		</tr>	
		

		
	</table> 

</div>

	<div class="user-info">
	<form method="post"> 
		<table> 
			<tr>	
				<th> 
					<label for="email">Account Email </label>
				</th>
				<td>	
					<input type="text" id="email" name="<?php echo SHARED_EMAIL; ?>">
				</td>
			</tr>

			<tr>	
				<th> 
					<label for="password"><?php echo $app[APP_NAME];  ?> Password </label>
				</th>
				<td>	
					<input type="text" id="password" name="<?php echo SHARED_PASSWORD; ?>">
				</td>
			</tr>

			<tr>	
				<th> 
					<label for="email-password">Email Password </label>
				</th>
				<td>	
					<input type="text" id="email-password" name="<?php echo EMAIL_PASSWORD; ?>">
				</td>
			</tr>	

			<tr>	
				<th> 
					<label for="end_date" >Subscription End Date </label>
				</th>
				<td>	
					<input maxlength="4"size="4"type="text" id="sub_date" name="<?php echo END_DATE; ?>">
				</td>
			</tr>	




			<tr>	
				<th> 
					<label for="last-four" >Last Four </label>
				</th>
				<td>	
					<input maxlength="4"size="4"type="text" id="last-four" name="last_four">
				</td>
			</tr>	
			
			<td><input type="submit"></td>

			
		</table> 
	</div>

</form> 