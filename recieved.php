

<?php 

	require_once("db/config.php");
	require_once("db/connect.php");
	require_once("inc/header.php"); 

?>

<link rel="stylesheet" type="text/css" href="css/message.css">

<div class="message"> 
<p> 
	<b> 
		Thank You for you email! We should get back to you within the next 24-hours. 
		We will be emailing the email address you provided at registration. If you are unsure as to which email 
		this is, please check your account info. 

		<br> 
		Be on the lookout 

		Thank you


	</b> 
</p> 
</div> 
